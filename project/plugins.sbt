// ---- NOTE THESE PLUGINS WILL NOT BE CHECKED IN THE OUTDATED DEPENDENCIES CHECK, DO MANUALLY ---
// Can also add these globally on machine at: Your global file at ~/.sbt/0.13/plugins/sbt-updates.sbt

 resolvers += Resolver.typesafeRepo("releases")


 // It appears that IntelliJ uses scalariform internally for formatting.
 //
addSbtPlugin("org.scalariform" % "sbt-scalariform" % "1.6.0")



// There is no way to easily check for updates here, so want to start reducing.
// AspectJ in particular is a pain in the butt

// https://github.com/jrudolph/sbt-dependency-graph
//
addSbtPlugin("net.virtual-void" % "sbt-dependency-graph" % "0.8.2")


//https://github.com/rtimush/sbt-updates
// sbt dependencyUpdates  shows which libraries have updates available on Maven/Ivy
// Main Task: sbt dependencyUpdates to see outdates libraries
addSbtPlugin("com.timushev.sbt" % "sbt-updates" % "0.1.10")


// Generic Native Packaging -- Used for Docker
// https://github.com/sbt/sbt-native-packager
addSbtPlugin("com.typesafe.sbt" % "sbt-native-packager" % "1.1.0")

// Using this for Kamon at the moment
// https://github.com/sbt/sbt-aspectj
addSbtPlugin("com.typesafe.sbt" % "sbt-aspectj" % "0.10.6")

// https://github.com/kamon-io/sbt-aspectj-runner
// aspectj-runner:run for load-time weaving.
// I don't think this gives us much that sbt-aspectj missing, except maybe runs without monitoring?
// Still needed?  Seems only for weaving in in dev mode, which I don't need so far.
addSbtPlugin("io.kamon" % "aspectj-runner" % "0.1.3")

// Was using this and Scalariform, need to sort out final solution
// http://www.scalastyle.org/sbt.html
//addSbtPlugin("org.scalastyle" %% "scalastyle-sbt-plugin" % "0.8.0")

