import com.typesafe.sbt.packager.docker.ExecCmd

name := "telenor-connect-gw"
version := "0.4-SNAPSHOT"
scalaVersion := "2.11.8"
packageDescription := " An Akka HTTP Server for Connect ID purposes."

scalacOptions ++= Seq(
                       "-unchecked",
                       "-deprecation",
                       "-Xlint",
                       "-Ywarn-dead-code",
                       "-language:_",
                       "-explaintypes",
                       "-target:jvm-1.8",
                       "-encoding", "UTF-8"
                     )



//resolvers += Resolver.typesafeRepo("releases")
resolvers += Resolver.sonatypeRepo("snapshots")
resolvers ++= Seq(
                   "Java.net Maven2 Repository" at "http://download.java.net/maven/2/",
                   "Maven Master" at "https://repo1.maven.org/maven2/"
                 )

mainClass in Compile := Some("com.odenzo.identification.gatewayserver.ServerMain")


libraryDependencies ++= Seq(
                             "com.typesafe" % "config" % "1.3.0" withSources() withJavadoc(), //  https://github.com/typesafehub/config
                             "net.ceedubs" %% "ficus" % "1.1.2" withSources() withJavadoc(),
                             "org.slf4j" % "slf4j-api" % "1.7.21" withSources() intransitive() withJavadoc(), // This is a dependancy, should skip
                             "ch.qos.logback" % "logback-classic" % "1.1.7" withSources() withJavadoc(),
                             "com.typesafe.scala-logging" %% "scala-logging" % "3.4.0" withSources() withJavadoc(),
                             //"codes.reactive" %% "scala-time" % "0.4.0-SNAPSHOT" withSources() withJavadoc(),
                             "org.json4s" %% "json4s-native" % "3.3.0" withSources() withJavadoc(), // https://github.com/json4s/json4s/
                             "org.json4s" %% "json4s-scalaz" % "3.3.0" withSources(), // Json4s Scalaz support
                             "org.scalaz" %% "scalaz-core" % "7.2.2" withSources() // For Either really

                           )


//The Slick Stack from slick.typesafe.com
libraryDependencies ++= {
  val slickVersion = "3.1.1"
  Seq(
       "com.typesafe.slick" %% "slick" % slickVersion withSources() withJavadoc(),
       "com.typesafe.slick" %% "slick-testkit" % slickVersion withSources() withJavadoc(),
       "com.typesafe.slick" %% "slick-hikaricp" % slickVersion withSources() withJavadoc(),
       "org.postgresql" % "postgresql" % "9.4.1208" withSources() withJavadoc() // This is JDBC Driver
     )
}


/** JWT Stuff For Authentication/Authorization: https://bitbucket.org/b_c/jose4j/wiki/Home */
libraryDependencies += "org.bitbucket.b_c" % "jose4j" % "0.5.1"


libraryDependencies ++= {
  val akkaVersion = "2.4.7"
  Seq(
       "com.typesafe.akka" %% "akka-actor" % akkaVersion withSources() withJavadoc(),
       "com.typesafe.akka" %% "akka-agent" % akkaVersion withSources() withJavadoc(),
       "com.typesafe.akka" %% "akka-kernel" % akkaVersion withSources() withJavadoc(),
       "com.typesafe.akka" %% "akka-persistence" % akkaVersion withSources() withJavadoc(),
       "com.typesafe.akka" %% "akka-slf4j" % akkaVersion withSources() withJavadoc(),
       "com.typesafe.akka" %% "akka-stream" % akkaVersion withSources() withJavadoc(),
       "com.typesafe.akka" %% "akka-http-core" % akkaVersion withSources() withJavadoc(),
       "com.typesafe.akka" %% "akka-http-experimental" % akkaVersion withSources() withJavadoc(),
       //"com.typesafe.akka" % "akka-http-xml-experimental_2.11" % akkaVersion withSources() withJavadoc(),

       "com.typesafe.akka" %% "akka-testkit" % akkaVersion % "test" withSources() withJavadoc(),
       "com.typesafe.akka" % "akka-http-testkit_2.11" % akkaVersion % "test" withSources() withJavadoc()
     )
}

// For json4s Marshalling in Akka-Http framework
// All json4s and scalaz json4s for a while (native instead of jackson)
libraryDependencies += "de.heikoseeberger" %% "akka-http-json4s" % "1.6.0"

/** ------------- Packaging ------------------------ **/

// Use sbt universal:package-bin  or package-zip-tarball
// or sbt universal:
// docker-compose.yml is expecting stuff in ./target/docker/stage/
enablePlugins(UniversalPlugin, JavaAppPackaging, DockerPlugin, UniversalDeployPlugin)


import com.typesafe.sbt.packager.docker._

dockerCommands := Seq()
dockerCommands := Seq(
                       Cmd("FROM", "java:8-jre"),
                       Cmd("MAINTAINER", "SteveF"),
                       Cmd("WORKDIR", "/opt"),
                       Cmd("ADD", "opt /opt"),
                       // Cmd("RUN", """["chown", "daemon:daemon","."]"""),
                       Cmd("EXPOSE", "8082"),
                       //Cmd("USER", "root"),
                       Cmd("ENTRYPOINT", """["/bin/bash"]"""),
                       ExecCmd("CMD",
                               "/opt/docker/bin/telenor-connect-gw",
                               "-Dconfig.resource=application-staging.conf"
                              )
                     )

/** ------------- Instrumentation wth Kamon with Graphite and Grafana (statsd based) ------------- **/

// Try the Docker Here: https://github.com/kamon-io/docker-grafana-graphite
// docker run -d -p 8888:80 -p 8125:8125/udp -p 8126:8126 --name kamon-grafana-dashboard kamon/grafana_graphite
// See seperate project for this cluster. Thi build is just concerned with sending
// to 8125 as statsd on localhost for now
libraryDependencies ++= {
  val version = "0.6.1"
  Seq(
       "io.kamon" %% "kamon-core" % version withSources(),
       "io.kamon" %% "kamon-log-reporter" % version withSources(),
       "io.kamon" %% "kamon-akka" % version withSources(),
       "io.kamon" %% "kamon-scala" % version withSources(),
       "io.kamon" %% "kamon-system-metrics" % version withSources(),
       "org.aspectj" % "aspectjweaver" % "1.8.9", // seems sbt-aspectjSettings dealing with this already. But need to
       // package
       // [Optiona Modules Belowl]
       "io.kamon" %% "kamon-statsd" % version withSources()
       // [Optional]
       //  "io.kamon" %% "kamon-datadog" % version withSources()


     )
}


// Bring the sbt-aspectj settings into this build for Kamon L
aspectjSettings

// For runtime weaving -- maybe disable when not mesing
javaOptions in run <++= AspectjKeys.weaverOptions in Aspectj

// We need to ensure that the JVM is forked for the
// AspectJ Weaver to kick in properly and do it's magic.
fork in run := true


/** ------------ Unit/Web/Stress Testing ------------------ **/


libraryDependencies ++= {
  Seq(
       "org.scalacheck" %% "scalacheck" % "1.13.1" % "test" withSources() withJavadoc(),
       "org.scalatest" % "scalatest_2.11" % "2.2.6" % "test" withSources() withJavadoc()
     )
}

//javaOptions in Test += "-Dconfig.file=conf/logger-test.xml"
testFrameworks += new TestFramework("org.scalameter.ScalaMeterFramework")
parallelExecution in Test := false
fork in test := false

//Revolver.settings


