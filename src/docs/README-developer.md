
Basically this is exercising the Connect ID related API from Telenor Digital.
Nothing much to look at....

There are some scattered X.md and X.puml documents around.
Essentially, going to make Akka-Streams for reactive data-flow approach

Instead of "session" we have some session state that will be stored in Actor.
e.g. authorize flows will be something like:
+ User Selects ConnectID Login to Do
+ Actor Created in Response to that Request and Given a "State Token" (and Name)
+ Redirect Sent Back to User
+ Some Time Later we Get a Callback/Redirect when login success/fail that has "State Token" init.

The time between Step 2 and Step 4 Callback may be in the range of 0.5 seconds to 5 minutes
Stateful actor can/will passivate... via recording/replay of events into transient datastore.
Not sure how this works with "non-actor" High Availability and two servers.
Pure way is (I Gues?) to route from Actor to State Actor no matter what machine its on.
If Actor has failed then regen it.
Cost of Actor failure here is very low, cost is really customer annoyance.

------

# Docker General Information

* See https://www.docker.com/ -- they have good documentation
* For OS X / Windows use Docker Toolbox -- download and install.


## Steve Local Stack Notes

https://github.com/kamon-io/docker-grafana-graphite 
Not sure why they hardcard base as: FROM     ubuntu:14.04.1    instead of latest which is now  14.04.3

docker pull kamon/grafana_graphite    (FROM ubuntu:14.04.1 :-()
docker pull java:8              (official and is JDK - FROM buildpack-deps:jessie-scm)
docker pull postgres            (official - FROM debian:jessie)
docker pull logstash            (official - FROM java:8-jre)
docker pull    


## Key Programming Frameworks and Technologies

* Scala with Akka, Akka-Http and Akka-Streams  
* Slick 3.x for DB stuff talking to a Postgres instance (seperate Docker) (overkill)
* Linux Based OS for deployment(s)
* Linux or OS X for development



## Development Setup

+ SBT based development using IntelliJ 14 

## Database Level -- One Vagrant Configured VM with Postgres
- Using CoreOS
- Postgres 9.4 Basic
- Need IP address and Host for This
- No Reporting or HA DB setup here for now
- See deploy/vagrant




### Working with Kamon and Graphics Portal

* Get Docker going (i.e. boot2docker up on OS X)
* Can use Kitematic (kamon/grafana_graphite) or checkout from git: kamon-io/docker-grafana-graphite
* docker run -d -p 80:80 -p 8125:8125/udp -p 8126:8126 --name kamon-grafana-dashboard kamon/grafana_graphite
