
-----


# Working/Hacking Notes 

+ Installed boot2docker via brew
+ Linked launchd and did launchctl to start immediately
+ Added `usedocker` alias in zsh to do the eval and set IP addresses etc
+ Installed vagrant via brew
+ Installed docker via brew
+ `boot2docker upgrade`
+ Installed Kitematic GUI for easiness
+ Installed docker-machine... lets met  create and install Dockers
+ Installed docker-compose (to deploy/build multi-container apps) (already installed by fig for Ripple stuff)




## Okay, now working on the actual Docker setup

# Postgres Docker
- https://registry.hub.docker.com/_/postgres/
- Note this pulls a lot of common stuff -- and its an "official" Docker we reuse
