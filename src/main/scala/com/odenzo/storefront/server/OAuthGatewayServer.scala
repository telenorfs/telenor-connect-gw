package com.odenzo.storefront.server

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.stream.ActorMaterializer
import com.odenzo.storefront.api.ServicesToDeploy
import com.odenzo.storefront.modules.connect.ConnectIdConfig
import com.typesafe.scalalogging.LazyLogging

/**
  * This is the gateway server exposed API layer.
  * In typical usage only the web application layer will call this, and it is hosted/exposed only to datacenter.
  * Moving to make it run in both HTTP and HTTPS mode.
  *
  * It makes calls to the ID server (Connect ID Server) external to the datacenter.
  *
  *
  */
object OAuthGatewayServer extends LazyLogging {
  implicit val system       = ActorSystem("oauth-server")
  implicit val materializer = ActorMaterializer()

  /**
    * A very crude way to start the server.... will keep running until killed
    */

  def startServer() {
    // VM Option -Dhttp.port=123 in IntelliJ (Not Command Line)

    val route = ServicesToDeploy.all

    val host = ConnectIdConfig.host
    val port = Option[String](System.getProperty("http.port"))
               .map(_.toInt)
               .getOrElse(ConnectIdConfig.httpPort)

    logger.warn(s"Starting OauthServer ${system.name} with Host: $host and Port: $port ... ")
    val loopback = java.net.InetAddress.getLoopbackAddress.getHostAddress
    logger.warn(s"Loopback Address: $loopback")
    logger.warn(s"Binding to Host : $host")
    val bindingFuture = Http().bindAndHandle(route, host, port)

    logger.warn(s"Server online at http://$host:$port/ping  ...")
    def shutdown(): Unit = {
      import system.dispatcher

      bindingFuture
      .flatMap(_.unbind()) // trigger unbinding from the port
      .onComplete(_ ⇒ system.terminate()) // and shutdown when done
    }
  }

}
