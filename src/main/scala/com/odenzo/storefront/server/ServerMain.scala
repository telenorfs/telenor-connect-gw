package com.odenzo.storefront.server

import com.typesafe.scalalogging.LazyLogging

/**
 * This is the production main entry point.
 * It will start a local server to mediate ConnectID calls with associated Kamon logging.
 */
object ServerMain extends App with LazyLogging {
  //Kamon.start()
  OAuthGatewayServer.startServer()
}
