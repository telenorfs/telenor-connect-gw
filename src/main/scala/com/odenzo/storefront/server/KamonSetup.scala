package com.odenzo.storefront.server

import com.typesafe.scalalogging.LazyLogging
import kamon.Kamon

object KamonSetup extends LazyLogging {

  val noMonitoring = true

  def start() {
    if (noMonitoring) {
      logger.warn("Kamon is turned off")
    } else {
      logger.warn("Starting Kamon -- daemonized -- you have to call stop!")
      Kamon.start()

      val someHistogram = Kamon.metrics.histogram("some-histogram")
      val someCounter = Kamon.metrics.counter("some-counter")

      someHistogram.record(42)
      someHistogram.record(50)
      someCounter.increment()
    }
  }

  def stop() {
    if (noMonitoring) {
      logger.warn("Not turning off Kamon because noMonitoring set to true in code")
    } else {
      logger.info("Shutting Down Kamon")
      Kamon.shutdown()
    }
  }
}
