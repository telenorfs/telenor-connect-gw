package com.odenzo.storefront.modules.cim.db

import slick.driver.PostgresDriver.api._
import slick.lifted.ProvenShape
import slick.lifted.Tag

case class UserIdMapping(local: String, connect: Option[String], facebook: Option[String])

class UserIdMappings(tag: Tag) extends Table[UserIdMapping](tag, Some("CIM"), "SOCIAL_ID_MAPPING") {

  def local = column[String]("localid", O.PrimaryKey, O.SqlType("varchar(50)"))

  def connect = column[Option[String]]("connectid", O.SqlType("varchar(30)"))

  def facebook = column[Option[String]]("facebook", O.SqlType("varchar(30)"))

  def * : ProvenShape[UserIdMapping] = (local, connect, facebook) <> (UserIdMapping.tupled, UserIdMapping.unapply)

  def connectIndex = index("idx_connect", connect, unique = true)

  def facebookIndex = index("idx_facebook", facebook, unique = true)
}

/**
 * More of an example and to show that DAO isn't needed except for note taking :-)
 */
private object userIds extends TableQuery(new UserIdMappings(_)) {
  val findByLocalId = this.findBy(_.local)
  val findByConnectId = this.findBy(_.connect)
  val findByFacebook = this.findBy(_.facebook)

  def update(user: UserIdMapping) = findByLocalId(user.local).update(user)

  def insert(user: UserIdMapping) = this += user

  def deleteByLocalId(localId: String) = findByLocalId(localId).delete

}

