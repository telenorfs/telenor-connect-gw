package com.odenzo.storefront.modules.cim.db

import slick.driver.PostgresDriver.api._
import slick.lifted.ProvenShape
import slick.lifted.Tag

/**
 *
 * @param id        Unique customer id -- this is the actual internal customer id.
 * @param firstName Their first name
 * @param lastName  Their last name
 */
case class Customer(id: String, firstName: String, lastName: String, email: String)

class Customers(tag: Tag) extends Table[Customer](tag, Some("CIM"), "CUSTOMER") {

  def id = column[String]("id", O.PrimaryKey, O.SqlType("varchar(50)"))

  def firstName = column[String]("first_name")

  def lastName = column[String]("last_name")

  def email = column[String]("email")

  def * : ProvenShape[Customer] = (id, firstName, lastName, email) <> (Customer.tupled, Customer.unapply)

  def idx = index("idx_email", email, unique = true)
}

/**
 * More of an example and to show that DAO isn't needed except for note taking :-)
 */
object the_customers extends TableQuery(new Customers(_)) {
  val findById = this.findBy(_.id)
  val findByEmail = this.findBy(_.email)

  def createCustomer(cust: Customer, connectid: Option[String]) = {
    // Create a customer record with new id
    // Disallow duplicate email
  }

}

