package com.odenzo.storefront.modules.cim.db

import com.typesafe.scalalogging.StrictLogging
import slick.driver.PostgresDriver.api._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
      import slick.driver.PostgresDriver.api._
import slick.lifted.ProvenShape
import slick.lifted.Tag
/**
 * This will be simple  interface exposing a few methods to database binding of Facebook and Connect ID
 * to the local user id.
 *
 */
trait CimDB {
  val db: Database = Database.forConfig("com.odenzo.connect.database")

  //
  //  // ToDo: Functor to do db.run with a Query and get Head Option
  //  def getSingleResult[T](query: TableQuery[T])(implicit db: Database) = {
  //    val res = db.run(query.result)
  //    res.map(rows ⇒ rows.headOption)
  //  }
}

trait IdMappings extends CimDB {

  def lookupUserIdMappingByConnect(connectId: String): Future[Option[UserIdMapping]] = {

    db.run(userIds.findByConnectId(Some(connectId)).result).map(_.headOption)
  }

  def lookupUserIdMappingByFacebook(facebookId: String): Future[Option[UserIdMapping]] = {
    db.run(userIds.findByFacebook(Some(facebookId)).result).map(_.headOption)
  }

  def lookupUserIdMappingById(localId: String): Future[Option[UserIdMapping]] = {
    db.run(userIds.findByLocalId(localId).result).map(_.headOption)
  }

  def updateUserIdMapping(user: UserIdMapping): Future[Int] = {
    db.run(userIds.update(user))
  }

  def createUserIdMapping(user: UserIdMapping): Future[Int] = {
    db.run(userIds.insert(user))
  }

  /**
   * Delete row matching localid
   *
   * @param localId
   * @return Count of rows deleted, 0 if no matching row, or 1 since deleting on PrimaryKey
   */
  def removeUserIdMapping(localId: String): Future[Int] = {
    db.run(userIds.deleteByLocalId(localId))
  }
}

trait CustomerDB extends CimDB {

  def findCustomerById(id: String) = {
    val query = the_customers.findById(id)
  }

  def createCustomer(c: Customer) = { db.run(the_customers += c) }
}

object CIM extends StrictLogging with IdMappings with CustomerDB {

  val schema = the_customers.schema ++ userIds.schema

  def dumpDDL() = {
    logger.info("Dumping DDL")
    schema.create.statements.foreach(x ⇒ logger.info(x))
    schema.drop.statements.foreach(x ⇒ logger.info(x))
  }

  def dropAll = { db.run(DBIO.seq(schema.drop)) }

  def createAll = { db.run(DBIO.seq(schema.create)) }

}
