package com.odenzo.storefront.modules.connect.gbe.id.accounts

import akka.http.scaladsl.model.{ HttpMethods, HttpRequest, StatusCodes, Uri }
import com.odenzo.storefront.modules.connect.ConnectIdConfig
import com.odenzo.storefront.modules.connect.gbe.TelenorConnectServiceClient
import com.odenzo.storefront.modules.connect.restutils._
import com.typesafe.scalalogging.LazyLogging
import org.json4s.JObject

import scala.concurrent.Future

object GetAllAccounts extends TelenorConnectServiceClient with ConnectIdConfig with LazyLogging {

  /**
   * Get all the accounts for a user,given `sub` , aka Connect ID
   */
  def get(connectId: String): Future[JObject] = {

    val request = HttpRequest(uri = Uri(s"$idServer/id/users/$connectId/accounts"))
      .withMethod(HttpMethods.GET)
      .withHeaders(comoyoServiceProviderAuthentication, AcceptJson)

    // Success Case
    fetchStrict(request).filter(_.status == StatusCodes.OK).flatMap(rs ⇒ extractFutureJson(rs)).map(_.get)
  }
}
