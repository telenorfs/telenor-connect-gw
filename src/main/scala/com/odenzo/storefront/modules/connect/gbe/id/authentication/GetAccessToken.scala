package com.odenzo.storefront.modules.connect.gbe.id.authentication

import akka.http.scaladsl.model.MediaTypes._
import akka.http.scaladsl.model.Uri.Query
import akka.http.scaladsl.model._
import com.odenzo.storefront.modules.connect.ConnectIdConfig
import com.odenzo.storefront.modules.connect.gbe.TelenorConnectServiceClient
import com.typesafe.scalalogging.LazyLogging
import org.json4s.JObject

import scala.concurrent.Future

/**
 * POST to  /oath/token with auth token to get access tokens and other basics
 * The joys of dynamic JSON where nobody knows what you will recieve for xmas.
 *
 */
object GetAccessToken extends TelenorConnectServiceClient with ConnectIdConfig with LazyLogging {

  def apply(authToken: String, callbackUrl:String) = getAccessToken(authToken, callbackUrl)

  /**
   * Given an auhtorization token from login redirect get an access token.
   * This cannot easily for tested, need a reset new token from ConnectID server
   *
   * @param authToken
   */
  def getAccessToken(authToken: String, callbackUrl:String): Future[JObject] = {
    //val service = "get_access_token"

    val formParams = Uri.Query(
      "grant_type" → "authorization_code",
      "code" → authToken,
      "redirect_uri" → callbackUrl,
      "client_id" → clientId
    )

    val entity = HttpEntity(ContentType(`application/x-www-form-urlencoded`, HttpCharsets.`UTF-8`), formParams.toString())

    val request = HttpRequest(uri = Uri(s"$oauthServer/oauth/token"))
      .withMethod(HttpMethods.POST)
      .withHeaders(serviceProviderAuthentication) // Required
      .withEntity(entity)

    logger.info("About to Fetch the Response: ")
    val response: Future[JObject] = fetchStrict(request).map { rs ⇒
      import scalaz.{ -\/, \/- }
      val ans = rs.status.intValue match {
        case 200 ⇒
          val ans = \/-(extractStrictJson(rs).get)
          logger.info("OK Response: " + ans.map(dump))
          ans

        case 400 ⇒
          logger.warn("NOT FOUND")
          val json = extractStrictJson(rs)
          val ans = -\/(new RestException("Downstream Server Reports Invalid Request - Most likely bad token", json))
          ans
        case 401 ⇒
          val json = extractStrictJson(rs)
          val ans = -\/(new RestException("Downstream Server Reports UnAuthorized Action", json))
          ans

        case other ⇒ -\/(new RestException(s"Unexpected Result Code $other", None))
      }

      ans.swap.foreach(e ⇒ throw e)
      ans.toOption.get
    }

    response
  }
}
