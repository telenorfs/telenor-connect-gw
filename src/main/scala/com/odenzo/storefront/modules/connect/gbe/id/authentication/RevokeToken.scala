package com.odenzo.storefront.modules.connect.gbe.id.authentication

import akka.http.scaladsl.model.Uri.Query
import akka.http.scaladsl.model.{ HttpMethods, HttpRequest, StatusCodes, Uri }
import com.odenzo.storefront.modules.connect.ConnectIdConfig
import com.odenzo.storefront.modules.connect.gbe.TelenorConnectServiceClient
import com.odenzo.storefront.modules.connect.gbe.id.accounts.GetAllAccounts._
import com.odenzo.storefront.modules.connect.restutils._
import com.typesafe.scalalogging.LazyLogging
import org.json4s.JObject

import scala.concurrent.Future

object RevokeToken extends TelenorConnectServiceClient with ConnectIdConfig with LazyLogging {

  /**
   * Revokes the given token.
   */
  def revoke(token: String): Future[JObject] = {

    val formParams = Uri.Query("token" → token)

    val request = HttpRequest(uri = Uri(s"$oauthServer/oauth/revoke"))
      .withMethod(HttpMethods.POST)
      .withHeaders(serviceProviderAuthentication)
      .withEntity(formPostEntity(formParams))

    // Success Case
    fetchStrict(request).filter(_.status == StatusCodes.OK).flatMap(rs ⇒ extractFutureJson(rs)).map(_.get)

  }

}
