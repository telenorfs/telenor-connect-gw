package com.odenzo.storefront.modules.connect

import com.typesafe.scalalogging.LazyLogging
import org.jose4j.jwt.JwtClaims
import org.jose4j.jwt.ReservedClaimNames
import org.jose4j.jwt.consumer.InvalidJwtException
import org.jose4j.jwt.consumer.JwtConsumerBuilder
import org.jose4j.jwt.consumer.JwtContext
import org.json4s._
import org.json4s.native._

import scala.collection.JavaConverters._

///**
// * In Connect ID  open_id scope returns an open id token with information.
// * We get this direct from the server, so just need to parse it.
// * There is means to get the public key to verify signing too.
// * (Is it signed or encrypted, we will see)
// *
// *
// * The token data returned is refferred to as JWE serialization format or something similar.
// *
// * This is all using Jose4j btw ... nice, but a decent Scala wrapper would be nice. Jose4s
// *
// */
object JwtUtils extends LazyLogging {
  // TODO: Transitioning the entire universe to Json4s (incrementally)
  // Unclear if this Jose4j in multi-thread safe  -- read docs
  val firstPassJwtConsumer = new JwtConsumerBuilder()
    .setSkipAllValidators()
    .setDisableRequireSignature()
    .setSkipSignatureVerification()
    .build()

  /** MT safe JWT Consumer **/
  def makeParser = {
    new JwtConsumerBuilder()
      .setSkipAllValidators()
      .setDisableRequireSignature()
      .setSkipSignatureVerification()
      .build()
  }

  def parse(jwt: String): JwtContext = makeParser.process(jwt)
  def claims(jwt: String): JwtClaims = parse(jwt).getJwtClaims

  /* ========= NEW CODE ========= */

  /**
   * Get the claims in my favorite (TBF) json format. Well, Spray Json for now.
   */
  def claimsAsJson(jwt: String) = claims(jwt).getClaimsMap

  def claimsAsJson(claims: JwtClaims) = JsonParser.parse(claims.toJson)

  // Defining a real function (not method)
  val claims = (x: JwtContext) ⇒ x.getJwtClaims

  // Converting a def to a (partial) function
  val parseFn: (String) ⇒ JwtContext = parse // Using (_, fixed,_) style to fix some of the parameters

  val composedFn = parseFn andThen claims andThen claimsAsJson // f andThen  g == g(f(x))  f compose g = f(g(x))

  def parseClaims(jwt: String): JValue = composedFn(jwt)

  /** Not using this **/
  def validate(jwt: String) = {

    logger.debug(s"Raw Token in Compact Serialization Format: $jwt")
    val jwtConsumer = new JwtConsumerBuilder()
      .setRequireExpirationTime() // the JWT must have an expiration time
      .setAllowedClockSkewInSeconds(30) // allow some leeway in validating time based claims to account for clock skew
      //      .setRequireSubject() // the JWT must have a subject claim
      //      .setExpectedIssuer("Issuer") // whom the JWT needs to have been issued by
      //      .setExpectedAudience("Audience") // to whom the JWT is intended for
      //      .setVerificationKey(rsaJsonWebKey.getKey()) // verify the signature with the public key
      .build() // create the JwtConsumer instance

    try {
      //  Validate the JWT and process it to the Claims
      val jwtClaims = jwtConsumer.processToClaims(jwt)
      logger.info("JWT validation succeeded! " + jwtClaims)
    } catch {
      case e: InvalidJwtException ⇒
        // InvalidJwtException will be thrown, if the JWT failed processing or validation in anyway.
        // Hopefully with meaningful explanations(s) about what went wrong.
        logger.error("Invalid JWT! ", e)
        throw e
    }
  }
}

// These are bogus, because dynamic fields based on algorithm etc.
case class JwtKeySet(keys: Seq[JwtKeys])
case class JwtKeys(kty: String, e: String, use: String, kid: String, alg: String, n: String)

//{
//    "kty":"RSA",
//    "e":"AQAB",
//    "use":"sig",
//    "kid":"ui_callback",
//    "alg":"RS256",
//    "n":"m48oqZCn8lncTGKVn_zR3MawcKU1tBpPJRY0z9N9hOLZZGXfNRc1Kmbvvqc4Iu2PMChVmwSEl5KYhrPIGlafVKhp-tP5f9hN6p68sejjWxlXqaGdN4IdPxsOgrRGYnfmRPdOMq_V6F_pkvwRzbFsNeTg5EApJqIZdbd0ulPbSDbQExBqWfg8SNgwoFrmvFby6ZS03iDK4rb5d7kBoazfhNL3I3ssbRi9FKr_YlnZ-QTff1xr5JjWJnP7wqA9JBEbAM3CUWoMuhjy1Snzx_ww1N9Yn2C4MnnhQnbE1yX05X-B3NUm0EH3VRj-JBtLJuwW_5qLpM3GPIwVoXmFANo2ww"
//  }

object JWToken extends LazyLogging {
  // TODO: Transitioning the entire universe to Json4s (incrementally)
  // Unclear if this Jose4j in multi-thread safe  -- read docs
  val validatingParser = new JwtConsumerBuilder()
    .setAllowedClockSkewInSeconds(10)
    .setEnableRequireEncryption()
    .build()

  /** MT safe JWT Consumer **/
  val unsafeParser = {
    new JwtConsumerBuilder()
      .setSkipAllValidators()
      .setDisableRequireSignature()
      .setSkipSignatureVerification()
      .build()
  }

  val stdClaimNames: Set[String] = ReservedClaimNames.INITIAL_REGISTERED_CLAIM_NAMES.asScala.toSet

  /* ========= NEW CODE ========= */

}

/**
 * See https://jwt.io
 * @param encodedToken The raw JWT token as a string
 */
class JWToken(val encodedToken: String) extends LazyLogging {

  private val context: JwtContext = JWToken.unsafeParser.process(encodedToken)

  def claims = context.getJwtClaims

  val stdClaims = JWToken.stdClaimNames.map(name ⇒ name → context.getJwtClaims.getClaimValue(name))

  def validate = JWToken.validatingParser.process(encodedToken)

  def asJson = JwtUtils.claimsAsJson(claims)
}
