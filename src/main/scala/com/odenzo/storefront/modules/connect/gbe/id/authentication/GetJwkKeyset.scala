package com.odenzo.storefront.modules.connect.gbe.id.authentication

import akka.http.scaladsl.model.{ HttpRequest, StatusCodes, Uri }
import com.odenzo.storefront.modules.connect.gbe.TelenorConnectServiceClient
import com.odenzo.storefront.modules.connect.gbe.id.accounts.RemoveAccount._
import com.typesafe.scalalogging.LazyLogging
import org.json4s.JObject

import scala.concurrent.Future

/**
 * JWK Keyset used to verify signing of open id id_token, only needed if not getting directly from Telenor Server.
 * Worth testing this with JWToken -- probably should be storing in database or cache on startup
 */
object GetJwkKeyset extends TelenorConnectServiceClient with LazyLogging {

  def fetch(): Future[JObject] = {
    val request = HttpRequest(uri = Uri(s"$oauthServer/oauth/public_keys.jwks"))

    // Success Case
    val response = fetchStrict(request)
    val ok = handleOkJson(response)
    ok
  }
}
