package com.odenzo.storefront.modules.connect.gbe.id.authentication

import akka.http.scaladsl.model.{ HttpRequest, StatusCodes, Uri }
import com.odenzo.storefront.modules.connect.gbe.TelenorConnectServiceClient
import com.typesafe.scalalogging.LazyLogging
import org.json4s.JObject

import scala.concurrent.Future

object VerifyToken extends TelenorConnectServiceClient with LazyLogging {

  /**
   * Check to see if access token is still valid.
   *
   * @param accessToken
   * @return Happy: Then access is verified. With JSON info in the slug structure  Sad: Error Json
   */
  def verifyAccessToken(accessToken: String): Future[Either[JObject, JObject]] = {
    val formParams = Uri.Query("access_token" → accessToken)
    verify(formParams)
  }

  def verifyIdToken(idToken: String): Future[Either[JObject, JObject]] = {
    val formParams = Uri.Query("id_token" → idToken)
    verify(formParams)
  }

  /**
   * Check to see if access token is still valid.
   *
   * @return Either[NotVerifiedJson, VerifiedJson] or an exception if system failure.
   */
  private def verify(queryParams: Uri.Query): Future[Either[JObject, JObject]] = {

    val request = HttpRequest()
      .withUri(Uri(s"$oauthServer/oauth/tokeninfo").withQuery(queryParams))
      .withDefaultHeaders(serviceProviderAuthentication)

    val response = fetchStrict(request)

    // ok means token is valid and get a ttl
    val ok = response.filter(r ⇒ r.status == StatusCodes.OK)
      .flatMap(rs ⇒ extractFutureJson(rs))
      .map(js ⇒ Right(js.get))

    // Return token not valid message from server in case it changes
    val notFound = response.filter(_.status == StatusCodes.NotFound)
      .flatMap(rs ⇒ extractFutureJson(rs))
      .map(js ⇒ Left(js.get))

    // Note that this can fail throwing an exception which is OK.
    val unexpected: Future[Either[JObject, JObject]] = response.flatMap(extractFutureJson)
      .map(json ⇒ throw new RestException("Unexpected Response Code From Server", json, rq = Some(request)))

    // Handle Known Response Codes and Unknown Response Code -- any exceptions get propagated
    val process = ok.fallbackTo(notFound).fallbackTo(unexpected)

    process
  }

}
