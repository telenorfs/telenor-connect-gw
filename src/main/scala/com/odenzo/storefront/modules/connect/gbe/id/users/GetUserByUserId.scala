package com.odenzo.storefront.modules.connect.gbe.id.users

import akka.http.scaladsl.model._
import com.odenzo.storefront.modules.connect.ConnectIdConfig
import com.odenzo.storefront.modules.connect.gbe.TelenorConnectServiceClient
import com.odenzo.storefront.modules.connect.gbe.id.emails.GetEmail._
import com.odenzo.storefront.modules.connect.restutils._
import com.typesafe.scalalogging.LazyLogging
import org.json4s.JObject

import scala.concurrent.Future

object GetUserByUserId extends TelenorConnectServiceClient with ConnectIdConfig with LazyLogging {

  /**
   * Given user's Connect Id  (sub) get the data for user. Mostly "primary" data.
   */
  def get(connectId: String): Future[JObject] = {

    // .withQuery(connectId)) TODO: Recheck this
    val request = HttpRequest(uri = Uri(s"$idServer/id/users/$connectId"))
      .withMethod(HttpMethods.GET)
      .withHeaders(serviceProviderAuthentication, AcceptJson)

    // Success Case
    fetchStrict(request).filter(_.status == StatusCodes.OK).flatMap(rs ⇒ extractFutureJson(rs)).map(_.get)

  }

}
