package com.odenzo.storefront.modules.connect.gbe.id.users

import akka.http.scaladsl.model.Uri.Query
import akka.http.scaladsl.model._
import com.odenzo.storefront.modules.connect.ConnectIdConfig
import com.odenzo.storefront.modules.connect.gbe.TelenorConnectServiceClient
import com.odenzo.storefront.modules.connect.gbe.id.emails.GetEmail._
import com.odenzo.storefront.modules.connect.restutils._
import com.typesafe.scalalogging.LazyLogging
import org.json4s.JObject

import scala.concurrent.Future

object GetUserByUserName extends TelenorConnectServiceClient with ConnectIdConfig with LazyLogging {

  /**
   * Given user id / user name, which is either phone number or email address.
   * This MFer does a 303 redirect... pain. Will change RestUtils to follow 303 redirects.
   *
   * @param refreshToken
   * @return
   */
  def exec(phoneOrEmail: String, accessToken: String): Future[JObject] = {

    // TODO: Verify accessToken not needed here and not useful.
    val q = Query(phoneOrEmail)

    // Need to write notes avout which one does the URLEncoding, @ should be encoded nah?
    // Parms? forget not. Mkae a test for it.
    val request = HttpRequest()
      .withUri(Uri(s"$idServer/id/users").withQuery(q)) // redirects to /id/info
      .withMethod(HttpMethods.GET)
      .withHeaders(comoyoServiceProviderAuthentication, AcceptJson)

    // Success Case
    fetchStrict(request).filter(_.status == StatusCodes.OK).flatMap(rs ⇒ extractFutureJson(rs)).map(_.get)

  }

}
