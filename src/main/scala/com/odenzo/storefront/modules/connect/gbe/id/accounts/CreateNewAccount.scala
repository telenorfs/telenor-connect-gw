package com.odenzo.storefront.modules.connect.gbe.id.accounts

import akka.http.scaladsl.model._
import com.odenzo.storefront.modules.connect.ConnectIdConfig
import com.odenzo.storefront.modules.connect.gbe.TelenorConnectServiceClient
import com.odenzo.storefront.modules.connect.restutils._
import com.typesafe.scalalogging.LazyLogging
import org.json4s.JObject

import scala.concurrent.Future

object CreateNewAccount extends TelenorConnectServiceClient with ConnectIdConfig with LazyLogging {

  /**
   * Creates a new account
   * I am forbidding hardlinking of the phone.
   */
  def create(connectId: String, bu: String, localUserId: String): Future[JObject] = {

    import org.json4s.JsonDSL._

    val json = ("type" → bu) ~ ("userid" → localUserId)
    val entity: RequestEntity = jsonPayload(json)

    val request = HttpRequest(uri = Uri(s"$idServer/id/users/$connectId/accounts"))
      .withMethod(HttpMethods.POST)
      .withHeaders(comoyoServiceProviderAuthentication, AcceptJson)
      .withEntity(entity)

    // Success Case
    fetchStrict(request).filter(_.status == StatusCodes.OK).flatMap(rs ⇒ extractFutureJson(rs)).map(_.get)

    //    RestUtils.fetchWithJson(request).map {
    //      case RestHappyCall(rq, rs, json: JObject) ⇒ RestHappyCall(rq, rs, json)
    //
    //      case sad: RestSadCall[_] if sad.status == 409 ⇒
    //        val longDesc = "An attempt was made to create an account that already exists. " +
    //          "This is due to either the {type,userid} tuple having been used before," +
    //          " or the msisdn already being set for another user."
    //        throw RestException(
    //          RestHandledError(sad, Seq(
    //            RestErrorMessage("409", s"Account $connectId Already Exists", longDesc)
    //          ))
    //        )
    //
    //      case other: RestJsonCall ⇒ throw RestException(other)
    //    }
  }

}
