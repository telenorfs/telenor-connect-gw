package com.odenzo.storefront.modules.connect.gbe.id.emails

import akka.http.scaladsl.model.{ HttpMethods, HttpRequest, StatusCodes, Uri }
import com.odenzo.storefront.modules.connect.ConnectIdConfig
import com.odenzo.storefront.modules.connect.gbe.TelenorConnectServiceClient
import com.odenzo.storefront.modules.connect.gbe.id.authentication.GetUserinfo._
import com.odenzo.storefront.modules.connect.restutils._
import com.typesafe.scalalogging.LazyLogging
import org.json4s.JObject

import scala.concurrent.Future

object GetListOfEmails extends TelenorConnectServiceClient with ConnectIdConfig with LazyLogging {

  /**
   * This seems to need SP Password, access token not good enough
   */
  def get(connectId: String): Future[JObject] = {

    val request = HttpRequest(uri = Uri(s"$idServer/id/users/$connectId/mails"))
      .withMethod(HttpMethods.GET)
      .withHeaders(comoyoServiceProviderAuthentication, AcceptJson)

    // Success Case
    fetchStrict(request).filter(_.status == StatusCodes.OK).flatMap(rs ⇒ extractFutureJson(rs)).map(_.get)

  }

}
