package com.odenzo.storefront.modules.connect.restutils

import akka.http.scaladsl.marshalling.Marshal
import akka.http.scaladsl.model._
import akka.http.scaladsl.model.headers.Accept
import akka.http.scaladsl.unmarshalling.Unmarshal
import akka.stream.Materializer
import com.typesafe.scalalogging.LazyLogging
import de.heikoseeberger.akkahttpjson4s.Json4sSupport
import org.json4s._
import org.json4s.native.JsonMethods

import scala.concurrent.{ ExecutionContext, Future }

/**
 * This is generic Json4s Utility and support for working with Akka-Http.
 * No application specific things in here please.
 */
trait AkkaJsonSupport extends Json4sSupport with LazyLogging {

  val AcceptJson = Accept(MediaTypes.`application/json`)

  implicit val serialization = org.json4s.native.Serialization
  implicit val formats = DefaultFormats
  implicit val prettyJson = Json4sSupport.ShouldWritePretty.True

  /**
   * A Hack -- since having trouble with Json4sSupport toEntity Marshaller
   *
   * @param json
   * @return
   */
  def jsonPayload(json: JObject): RequestEntity = HttpEntity(ContentTypes.`application/json`, JsonMethods.pretty(JsonMethods.render(json)))

  def marshallJSON(json: JObject)(implicit ec: ExecutionContext): Future[HttpEntity] = Marshal(json).to[HttpEntity]

  /**
   * Tries to unmarshall JSON entity in HttpResponse to JObject.
   *
   * @param rs
   * @param ec
   * @return Some(JObject) with payload of request in it verbatim or None if there is some failure (or not present)
   */
  def extractFutureJson(rs: HttpResponse)(implicit ec: ExecutionContext, mat: Materializer): Future[Option[JObject]] = {
    val res = Unmarshal(rs).to[JObject].map(Some(_)).recover {
      case e: Exception ⇒ None
    }
    res
  }

  def extractStrictJson(rs: HttpResponse)(implicit ec: ExecutionContext, mat: Materializer): Option[JObject] = {
    rs.entity match {
      case entity: HttpEntity.Strict ⇒
        logger.info("Strict Entity")
        val in: JsonInput = StringInput(entity.data.utf8String)
        JsonMethods.parseOpt(in).map(v ⇒ v.asInstanceOf[JObject])

      case other ⇒
        logger.warn(s"Response Entity Not Strict... go away $other")
        throw new IllegalArgumentException("extractJson needs a Strict Entity")
    }
  }

  def dump(oj: Option[JValue]): String = oj.fold("No JSON Return")(dump(_))
  def dump(j: JValue): String = JsonMethods.pretty(JsonMethods.render(j))

  def extractJson(res: HttpResponse): Option[JObject] = {
    res.entity match {
      case entity: HttpEntity.Strict ⇒
        logger.info("Strict Entity")
        val in: JsonInput = StringInput(entity.data.utf8String)
        JsonMethods.parseOpt(in).map(v ⇒ v.asInstanceOf[JObject])

      case other ⇒
        logger.warn(s"Response Entity Not Strict... go away $other")
        throw new IllegalArgumentException("extractJson needs a Strict Entity")
    }

  }

}
