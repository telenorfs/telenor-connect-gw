package com.odenzo.storefront.modules.connect.gbe

/**
 * http://docs.telenordigital.com/apis/connect/id/accounts.html
 * This is handy functionality to bind local account data with global id, stored on the Connect ID backend.
 */
package object accounts {

}
