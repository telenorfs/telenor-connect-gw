package com.odenzo.storefront.modules.connect.gbe.id.emails

import akka.http.scaladsl.model.{ HttpMethods, HttpRequest, StatusCodes, Uri }
import com.odenzo.storefront.modules.connect.ConnectIdConfig
import com.odenzo.storefront.modules.connect.gbe.TelenorConnectServiceClient
import com.odenzo.storefront.modules.connect.gbe.id.authentication.RevokeToken._
import com.odenzo.storefront.modules.connect.restutils._
import com.typesafe.scalalogging.LazyLogging
import org.json4s.JObject

import scala.concurrent.Future

/**
 * Gets the details of one particular email address (object) attached
 * to the Connect ID.
 * I think the precondition is that the Connect ID user has to have
 * granted permissions or just to my general SP grant?
 * This just seems to get the same object data as GetListOfEmails....
 */
object GetEmail extends TelenorConnectServiceClient with ConnectIdConfig with LazyLogging {

  /**
   * This seems to need SP Password, access token not good enough
   */
  def get(connectId: String, mailId: String): Future[JObject] = {

    //val bearerToken: HttpHeader = Authorization(OAuth2BearerToken(accessToken))

    val request = HttpRequest(uri = Uri(s"$idServer/id/users/$connectId/mails/$mailId"))
      .withMethod(HttpMethods.GET)
      .withHeaders(comoyoServiceProviderAuthentication, AcceptJson)

    // Success Case
    fetchStrict(request).filter(_.status == StatusCodes.OK).flatMap(rs ⇒ extractFutureJson(rs)).map(_.get)

  }

}
