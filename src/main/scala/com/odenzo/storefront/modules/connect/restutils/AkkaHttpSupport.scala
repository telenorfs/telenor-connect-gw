package com.odenzo.storefront.modules.connect.restutils

import akka.http.scaladsl.marshalling.{ ToResponseMarshallable, ToResponseMarshaller }
import akka.http.scaladsl.model.HttpEntity.Strict
import akka.http.scaladsl.model.MediaTypes._
import akka.http.scaladsl.model.Uri.Query
import akka.http.scaladsl.model._
import akka.http.scaladsl.server.{ Directives, Route }
import com.typesafe.scalalogging.LazyLogging
import org.json4s._
import org.json4s.native.JsonMethods

import scala.concurrent.{ ExecutionContext, Future }

trait AkkaHttpSupport {


  class RestException(
    msg: String,
    val json: Option[JObject],
    cause: Exception = null,
    val rq: Option[HttpRequest] = None,
    val rs: Option[HttpResponse] = None
  )
      extends Exception(msg, cause) with LazyLogging {

    override def getMessage: String = super.getMessage + json.fold("[No JSON]")(j ⇒ JsonMethods.pretty(JsonMethods.render(j)))

  }

  /**
   * Missing utility function in akka-http so far... or I can't find easy way to do.
    * But I am sure there is one.
   *
   * @param query
   * @return
   */
  def formPostEntity(query: Query): Strict = {
    HttpEntity(
      ContentType(`application/x-www-form-urlencoded`, HttpCharsets.`UTF-8`),
      query.toString() // This will urlencode
    )
  }

}

trait AkkaHttpDirectives extends Directives with AkkaJsonSupport {

  implicit def executionContext: ExecutionContext


  def complete[T: ToResponseMarshaller](resource: Future[Option[T]]): Route =
    onSuccess(resource) {
      case Some(t) ⇒ complete(ToResponseMarshallable(t))
      case None ⇒ complete(404 -> "Could not Find Your Entity.")
    }

  //def complete(resource: Future[Unit]): Route = onSuccess(resource) { complete(204, None) }

}
