package com.odenzo.storefront.modules.connect.gbe.id.authentication

import akka.http.scaladsl.model._
import akka.http.scaladsl.model.headers.{ Accept, Authorization, OAuth2BearerToken }
import com.odenzo.storefront.modules.connect.ConnectIdConfig
import com.odenzo.storefront.modules.connect.gbe.TelenorConnectServiceClient
import com.odenzo.storefront.modules.connect.gbe.id.accounts.GetAllAccounts._
import com.odenzo.storefront.modules.connect.restutils._
import com.typesafe.scalalogging.LazyLogging
import org.json4s.JObject

import scala.concurrent.Future

object GetUserinfo extends TelenorConnectServiceClient with ConnectIdConfig with LazyLogging {

  def apply(accessToken: String): Future[JObject] = getUserinfo(accessToken)

  /**
   * Given access token this will return data depending on the grants given.
   * Typically:
   *  {
   * "phone_number_verified": true,
   * "phone_number": "66891261996",
   * "email_verified": true,
   * "email": "test@odenzo.com",
   * "sub": "6033966863208161280"
   * }
   *
   * @param refreshToken
   * @return
   */
  def getUserinfo(accessToken: String): Future[JObject] = {

    val auth = Authorization(OAuth2BearerToken(accessToken))
    val accept = Accept(MediaTypes.`application/json`)

    val request = HttpRequest(uri = Uri(s"$oauthServer/oauth/userinfo"))
      .withMethod(HttpMethods.GET)
      .withHeaders(auth, accept)

    // Success Case
    fetchStrict(request).filter(_.status == StatusCodes.OK).flatMap(rs ⇒ extractFutureJson(rs)).map(_.get)
  }

}
