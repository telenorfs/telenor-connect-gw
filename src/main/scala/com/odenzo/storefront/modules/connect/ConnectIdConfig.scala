package com.odenzo.storefront.modules.connect

import akka.http.scaladsl.model.headers.Authorization
import akka.http.scaladsl.model.headers.BasicHttpCredentials
import com.typesafe.config.Config
import com.typesafe.config.ConfigFactory
import com.typesafe.config.ConfigRenderOptions
import com.typesafe.scalalogging.LazyLogging

trait ConnectIdConfig extends LazyLogging {

  import net.ceedubs.ficus.Ficus._

  // Why?
  System.setProperty("http.proxyHost", "127.0.0.1")
  System.setProperty("http.proxyPort", "8888")

  // Switch to determining mode by System property or Environment Variable
  val propertyMode = sys.props.get("ODENZO_RUN_MODE")
  val envMode = sys.env.get("ODENZO_RUN_MODE")
  val runMode = propertyMode.getOrElse(envMode.getOrElse("dev"))

  logger.warn(s"Property Mode $propertyMode \t Env Mode: $envMode -- Using $runMode")

  val configRenderOptions = ConfigRenderOptions.defaults()
    .setJson(false)
    .setComments(true)
    .setFormatted(true)
  //.setOriginComments(true)

  // This fails on docker and works on OS X -- the default application.conf is loaded though
  val configByMode = ConfigFactory.load(s"application-$runMode.conf")

  val serverConfig = configByMode.getConfig("com.odenzo.connect-gw.server")
  val httpPort = serverConfig.getAs[Int]("http.port").getOrElse(8082)
  val host = serverConfig.getAs[String]("http.server").getOrElse("https://localhost/fromdefault")
  val hostTLS = serverConfig.getAs[Boolean]("http.ssl").getOrElse(true)
  val httpServerUrl = if (hostTLS) s"https://$host" else s"http://$host:$httpPort"

  /**
   * Configuration rooted at telenor.connect.auth
   */
  val connectConfig = configByMode.getConfig("telenor.connect.auth")

  /**
   * This has to be exactly as registered with Telenor Connect.
   * Must match the callback used to get Auth Token for instance.
   * Note: This app doesn't actually receive any callbacks at the URL
   */
  val oauthCallback = connectConfig.as[String]("oauthCallback")
  logger.warn(s"Using $oauthCallback which much match front-end callback URL")
  val oauthServer = connectConfig.as[String]("server.oath")
  val idServer = connectConfig.as[String]("server.id")
  val paymentsServer = connectConfig.as[String]("server.payments")
  val clientId = connectConfig.as[String]("client_id")

  val serviceProviderAuthentication = {
    val user = clientId
    val password = configByMode.as[String]("telenor.connect.auth.client_password")
    if (password.startsWith("--")) {
      throw new RuntimeException(s"The password is not being set correctly $password")
    }
    //  logger.warn(s"SECURITY: Auth $user   $password")
    Authorization(BasicHttpCredentials(user, password))
  }

  val comoyoServiceProviderAuthentication = {
    val user = configByMode.as[String]("telenor.connect.comoyo.client_id")
    val password = configByMode.as[String]("telenor.connect.comoyo.client_password")
    // logger.warn(s"SECURITY: Comoyo $user   $password")
    Authorization(BasicHttpCredentials(user, password))
  }
}

object ConnectIdConfig extends ConnectIdConfig with LazyLogging {

  val odenzoConfig = configByMode.withOnlyPath("odenzo")
  val telenorConfig = configByMode.withOnlyPath("telenor")

  // logger.debug(s"SECURITY: Telenor Config By Mode $runMode -->  ${configToString(telenorConfig)}")

  def configToString(c: Config): String = {
    val renderOpts = ConfigRenderOptions.defaults().setOriginComments(false).setComments(false).setJson(false)
    c.root().render(renderOpts)
  }
}
