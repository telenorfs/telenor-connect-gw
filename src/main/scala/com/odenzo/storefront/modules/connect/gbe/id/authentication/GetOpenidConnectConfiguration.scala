package com.odenzo.storefront.modules.connect.gbe.id.authentication

import akka.http.scaladsl.model.{ HttpRequest, Uri }
import com.odenzo.storefront.modules.connect.gbe.TelenorConnectServiceClient
import com.odenzo.storefront.modules.connect.restutils._
import com.typesafe.scalalogging.LazyLogging
import org.json4s.JObject

import scala.concurrent.Future

object GetOpenidConnectConfiguration extends TelenorConnectServiceClient with LazyLogging {

  /**
   * Check to see if access token is still valid.
   *
   * @param accessToken
   * @return Happy: Then access is verified. With JSON info in the slug structure  Sad: Error Json
   */
  def apply(): Future[JObject] = {
    val request = HttpRequest(uri = Uri(s"$oauthServer/oauth/.well-known/openid-configuration"))
    // Success Case
    val response = fetchStrict(request)
    val ok = handleOkJson(response)
    ok
  }

}
