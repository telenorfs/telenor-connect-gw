package com.odenzo.storefront.modules.connect.gbe.id.users

import akka.http.scaladsl.model._
import com.odenzo.storefront.modules.connect.ConnectIdConfig
import com.odenzo.storefront.modules.connect.gbe.TelenorConnectServiceClient
import com.odenzo.storefront.modules.connect.restutils._
import com.typesafe.scalalogging.LazyLogging
import org.json4s.JObject

import scala.concurrent.Future

/**
 * TODO: SendMessage service not developed or tested
 */
//object SendMessage extends TelenorConnectServiceClient with ConnectIdConfig with LazyLogging {
//
//  case class MessageInfo(
//      locale:       Option[String],
//      brand:        Option[String],
//      channel:      Option[String],
//      sourceName:   Option[String],
//      triggerName:  Option[String],
//      templateName: Option[String],
//      subject:      Option[String],
//      message:      Option[String],
//      substitions:  Option[String]
//  ) {
//    // Validation Needed
//    // channel= {mail,sms or None for now, via App would be good!}
//    // templateName XOR message etc..
//    // See:    http://docs.telenordigital.com/apis/connect/id/users.html
//
//  }
//
//  /**
//   * Given user id / user name, which is either phone number or email address
//   * @param refreshToken
//   * @return
//   */
//  def exec(connectId: String, msg: MessageInfo, accessToken: String): Future[RestHappyCall[JObject]] = {
//
//    /// 415 received with SP authorization only -- unsupported media type with app/json
//    //
//    val formParams = convertToFormParams(msg)
//    val json = convertToJson(msg)
//    val request = HttpRequest(uri = Uri(s"$idServer/id/users/$connectId/message"))
//      .withMethod(HttpMethods.POST)
//      .withEntity(msg)
//      .withHeaders(comoyoServiceProviderAuthentication, AcceptJson)
//
//    RestUtils.fetchWithJson(request).map {
//      case RestHappyCall(rq, rs, json: JObject) ⇒ RestHappyCall(rq, rs, json)
//      case other: RestJsonCall                   ⇒ throw RestException(other)
//    }
//  }
//
//  private def case2Map(msg: MessageInfo): Map[String, String] = {
//    Seq(
//      msg.brand.map("brand" → _),
//      msg.channel.map("channel" → _),
//      msg.locale.map("locale" → _),
//      msg.message.map("message" → _),
//      msg.sourceName.map("sourceName" → _),
//      msg.subject.map("subject" → _),
//      msg.templateName.map("templateName" → _),
//      msg.triggerName.map("triggerName" → _)
//    ).flatten.toMap
//  }
//
//  private def convertToFormParams(msg: MessageInfo) = FormData(case2Map(msg))
//
//
//
//}
