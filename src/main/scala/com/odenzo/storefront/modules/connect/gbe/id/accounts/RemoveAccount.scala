package com.odenzo.storefront.modules.connect.gbe.id.accounts

import akka.http.scaladsl.model._
import com.odenzo.storefront.modules.connect.ConnectIdConfig
import com.odenzo.storefront.modules.connect.gbe.TelenorConnectServiceClient
import com.odenzo.storefront.modules.connect.restutils._
import com.typesafe.scalalogging.LazyLogging
import org.json4s.JObject

import scala.concurrent.Future

object RemoveAccount extends TelenorConnectServiceClient with ConnectIdConfig with LazyLogging {

  /**
   * Deletes a previously created account
   * 204 on deletion, or if the account doesn't already exist.
   * 404 on not finding connect id
   */
  def delete(connectId: String, accountId: String): Future[JObject] = {

    val request = HttpRequest(uri = Uri(s"$idServer/id/users/$connectId/accounts/$accountId"))
      .withMethod(HttpMethods.DELETE)
      .withHeaders(comoyoServiceProviderAuthentication, AcceptJson)

    // Success Case
    val response = fetchStrict(request)
    val ok = handleOkJson(response)

    // TODO: Handle fallback cases
    ok

  }

}
