package com.odenzo.storefront.modules.connect.gbe.id.authentication

import akka.http.scaladsl.model.Uri.Query
import akka.http.scaladsl.model.{ HttpMethods, HttpRequest, StatusCodes, Uri }
import com.odenzo.storefront.modules.connect.ConnectIdConfig
import com.odenzo.storefront.modules.connect.gbe.TelenorConnectServiceClient
import com.odenzo.storefront.modules.connect.restutils._
import com.typesafe.scalalogging.LazyLogging
import org.json4s.JObject

import scala.concurrent.Future

object RefreshAccessToken extends TelenorConnectServiceClient with ConnectIdConfig with LazyLogging {

  /**
   * This takes a refresh access token and the SP (ClientId, Password) pair.
   *
   * @param refreshToken
   * @return
   */
  def refreshAccessToken(refreshToken: String): Future[JObject] = {

    val formParams = Uri.Query(
      "grant_type" → "refresh_token",
      "refresh_token" → refreshToken,
      "client_id" → clientId
    )

    val request = HttpRequest(uri = Uri(s"$oauthServer/oauth/token"))
      .withMethod(HttpMethods.POST)
      .withHeaders(serviceProviderAuthentication)
      .withEntity(formPostEntity(formParams))

    // Success Case
    fetchStrict(request).filter(_.status == StatusCodes.OK).flatMap(rs ⇒ extractFutureJson(rs)).map(_.get)
  }

}
