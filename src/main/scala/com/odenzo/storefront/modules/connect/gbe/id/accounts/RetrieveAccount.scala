package com.odenzo.storefront.modules.connect.gbe.id.accounts

import akka.http.scaladsl.model.{ HttpMethods, HttpRequest, StatusCodes, Uri }
import com.odenzo.storefront.modules.connect.ConnectIdConfig
import com.odenzo.storefront.modules.connect.gbe.TelenorConnectServiceClient
import com.typesafe.scalalogging.LazyLogging
import org.json4s.JObject

import scala.concurrent.Future

object RetrieveAccount extends TelenorConnectServiceClient with ConnectIdConfig with LazyLogging {

  /**
   * Get account information for the given connectid and accountid object
   */
  def get(connectId: String, account: String): Future[JObject] = {

    val request = HttpRequest(uri = Uri(s"$idServer/id/users/$connectId/accounts/$account"))
      .withMethod(HttpMethods.GET)
      .withHeaders(comoyoServiceProviderAuthentication, AcceptJson)

    // Success Case
    val response = fetchStrict(request)
    val ok = handleOkJson(response)

    ok
  }

}
