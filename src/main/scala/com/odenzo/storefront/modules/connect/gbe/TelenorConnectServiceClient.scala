package com.odenzo.storefront.modules.connect.gbe

import java.util.concurrent.TimeUnit

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.headers.{ Authorization, OAuth2BearerToken }
import akka.http.scaladsl.model.{ HttpResponse, _ }
import akka.stream.ActorMaterializer
import com.odenzo.storefront.modules.connect.ConnectIdConfig
import com.odenzo.storefront.modules.connect.restutils._
import com.typesafe.scalalogging.LazyLogging
import org.json4s.JObject

import scala.concurrent.duration.FiniteDuration
import scala.concurrent.{ ExecutionContext, Future }

trait TelenorConnectServiceClient extends LazyLogging with AkkaHttpSupport with AkkaJsonSupport with ConnectIdConfig {

  implicit val system = ActorSystem("connect-gbe-clients")
  implicit val materializer: ActorMaterializer = ActorMaterializer()
  implicit val ec = ExecutionContext.global // Or from dispatcher, need to think


  /**
   * Converts a string to an HTTPHeader of type Authorization in Oauth2 bearer token format.
   */
  def string2oauthheader(t: String) = Authorization(OAuth2BearerToken(t))

  def fetchStrict(request: HttpRequest)(implicit system: ActorSystem, materializer: ActorMaterializer): Future[HttpResponse] = {

    logger.debug(s"REQUESTING: $request")
    val rs = Http().singleRequest(request).flatMap(_.toStrict(FiniteDuration(10, TimeUnit.SECONDS)))
    rs.onSuccess { case r ⇒ logger.debug(s"RESPONSE: $r") }
    rs.onFailure { case e ⇒ logger.error("Failed Response", e) }
    rs
  }

  /**
   * Filters the given response on 200 (OK) HTTP Response Code and extract JSON Body.
   * Throws exception if JSON not extractable.
   *
   * @param rs
   * @return
   */
  def handleOkJson(rs: Future[HttpResponse]): Future[JObject] = {
    rs.filter(_.status == StatusCodes.OK)
      .flatMap(r ⇒ extractFutureJson(r))
      .map(_.get)
  }
}
