package com.odenzo.storefront.api

import akka.actor.ActorSystem
import akka.http.scaladsl.model._
import akka.http.scaladsl.server.Directives._
import akka.stream.ActorMaterializer
import com.odenzo.storefront.api.LocalDbServices.DBError
import com.odenzo.storefront.modules.cim.db.CIM
import com.odenzo.storefront.modules.cim.db.UserIdMapping
import com.odenzo.storefront.modules.connect.restutils._
import com.typesafe.scalalogging.LazyLogging
import org.postgresql.util.PSQLException

import scala.language.implicitConversions
import scala.util.Failure
import scala.util.Success

/**
 * Following the basic approach of
 * POST --> Create      If alrady exists will reject I think.
 * GET --> Read
 * PUT --> Update       I doesn't exist already then? (ie update or upsert?)
 * DELETE --> Delete
 *
 *
 */
object AdminServices extends LazyLogging with AkkaHttpSupport with AkkaJsonSupport {

  implicit val system = ActorSystem("private-services")
  implicit val materializer = ActorMaterializer()

  /*=== Start of all the Route definitions ===*/
  val ping = path("/admin/ping") {
    get {
      logger.info("Got A PING request")
      complete("answer hello from  akka-http")
    }
  }

  /**
   * Drops all the standard tables for dev screwing around
   */
  val create = post {
    path("admin" / "droptables") {
      entity(as[UserIdMapping]) { newUser ⇒ // What happens if Marsheller fails, rejection
        onComplete(CIM.createUserIdMapping(newUser)) {
          case Success(u) ⇒ complete(StatusCodes.Created -> newUser) // Should use the one from DB
          case Failure(ex: PSQLException) ⇒ complete(StatusCodes.BadRequest -> DBError.from(ex))
          case Failure(e: Throwable) => throw e
        }
      }
    }
  }

  val all = ping ~ create
}
