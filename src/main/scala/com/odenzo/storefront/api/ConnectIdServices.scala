package com.odenzo.storefront.api

import akka.actor.ActorSystem
import akka.http.scaladsl.model.StatusCodes
import akka.stream.ActorMaterializer
import com.odenzo.storefront.modules.connect.JWToken
import com.odenzo.storefront.modules.connect.gbe.id.authentication.GetAccessToken.RestException
import com.odenzo.storefront.modules.connect.gbe.id.authentication.GetAccessToken
import com.odenzo.storefront.modules.connect.gbe.id.authentication.GetUserinfo
import com.odenzo.storefront.modules.connect.restutils._
import com.typesafe.scalalogging.LazyLogging
import org.json4s.JsonDSL._
import org.json4s.JObject
import org.json4s._

import scala.concurrent.Future
import scala.language.implicitConversions
import scala.language.postfixOps
import scala.util.Failure
import scala.util.Success

object ConnectIdServices extends LazyLogging with AkkaHttpSupport with AkkaJsonSupport {
  implicit val system = ActorSystem("private-services")
  implicit val materializer = ActorMaterializer()

  // So I can have easy access to different Execution Context if needed
  import akka.http.scaladsl.server.Directives._

  /**
   * Called by Web layer after it successfully receives a redirect on Connect ID login.
   * This gets access and refresh token and decodes JWT and does a call to user info.
   */
  val connectLogin = path("client" / "resolve_auth_token") {
    get {
      logRequestResult("connect_login") {
        parameters("code","cb_url") { (code, callbackUrl) ⇒
          onComplete(fetchAccessToken(code, callbackUrl)) {
            case Success(json) ⇒ complete(StatusCodes.OK → json)
            case Failure(e: RestException) ⇒ complete(StatusCodes.BadRequest → e.json)
            case Failure(e: Exception) ⇒ complete(StatusCodes.InternalServerError -> e.getMessage)
            case Failure(e: Throwable) => throw e
          }
        }
      }
    }
  }

  val fetchUserInfo = path("client" / "userinfo") {
    get {
      logRequestResult("connect_login") {
        parameters("access") { (token) ⇒
          onComplete(getUserInfo(token)) {
            case Success(json) ⇒ complete(StatusCodes.OK → json)
            case Failure(e: RestException) ⇒ complete(StatusCodes.BadRequest → e.json)
            case Failure(e: Exception) ⇒ complete(StatusCodes.InternalServerError -> e.getMessage)
            case Failure(e: Throwable) => throw e
          }
        }
      }
    }
  }

  /**
   *
   * @param authToken Valid authorization token, typically from login redirect
   * @return Full json with access and refresh tokens per standard (e.g. json \ "access_token")
   */
  def fetchAccessToken(authToken: String, callbackUrl:String): Future[JObject] = {
    import scala.concurrent.ExecutionContext.Implicits.global
    GetAccessToken.getAccessToken(authToken, callbackUrl).map { json =>
      val jwtJson = extractAndValidateJWT(json).getOrElse(JNothing)
      json ~ JField("jwt", jwtJson)
    }
  }

  protected def extractAndValidateJWT(json: JObject): Option[JValue] = {
    (json \ "id_token").extractOpt[String].map { encodedtoken ⇒
      val token = new JWToken(encodedtoken)
      logger.info("JWT Details in JSON: " + token)
      token.asJson
    }
  }

  protected def getUserInfo(accessToken: String) = {
    val fullJson = GetUserinfo(accessToken)
    fullJson
  }

  val all = connectLogin ~ fetchUserInfo

}
