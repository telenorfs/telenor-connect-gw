package com.odenzo.storefront.api

import akka.http.scaladsl.server.Directives._

import scala.language.implicitConversions

object ServicesToDeploy {
  val all = ConnectIdServices.all ~ LocalDbServices.all ~ AdminServices.all
}
