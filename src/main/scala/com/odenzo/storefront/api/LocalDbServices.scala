package com.odenzo.storefront.api

import akka.actor.ActorSystem
import akka.http.scaladsl.model._
import akka.http.scaladsl.server.Directives._
import akka.stream.ActorMaterializer
import com.odenzo.storefront.modules.cim.db.CIM
import com.odenzo.storefront.modules.cim.db.UserIdMapping
import com.odenzo.storefront.modules.connect.restutils._
import com.typesafe.scalalogging.LazyLogging
import org.postgresql.util.PSQLException

import scala.language.implicitConversions
import scala.util.Failure
import scala.util.Success

/**
 * Following the basic approach of
 * POST --> Create      If alrady exists will reject I think.
 * GET --> Read
 * PUT --> Update       I doesn't exist already then? (ie update or upsert?)
 * DELETE --> Delete
 *
 *
 */
object LocalDbServices extends LazyLogging with AkkaHttpSupport with AkkaJsonSupport {
  // TODO: Make error responses an array for future validation and multiple errors handling
  // TODO: Address leakage of security related information in errors

  implicit val system = ActorSystem("private-services")
  implicit val materializer = ActorMaterializer()


  /**
   * Idea here is to wrap in AST with DBError and PublicDBError that doesn't expose too much info as needed
   * Let Marshaller take care
   *
   * @param msg      Message straight from the ANSI SQL Error
   * @param sqlState SQL State per ANSI SQL Definition
   */
  case class DBError(error: String, sqlState: String, msg: String = "") {
    def withMessage(msg: String) = this.copy(msg = msg)
  }

  object DBError {
    def from(e: PSQLException) = {
      DBError(e.getMessage, e.getSQLState)
    }

  }

  /**
   * PUT is to  update existing, POST can be used to create a new record
   * with semantics that if a record exists then do not create
   */
  val create = post {
    path("users") {
      entity(as[UserIdMapping]) { newUser ⇒ // What happens if Marsheller fails, rejection
        onComplete(CIM.createUserIdMapping(newUser)) {
          case Success(u) ⇒ complete(StatusCodes.Created -> newUser) // Should use the one from DB
          case Failure(ex: PSQLException) ⇒ complete(StatusCodes.BadRequest -> DBError.from(ex))
          case Failure(e: Throwable) => throw e
        }
      }
    }
  }

  val read = get {
    path("users" / Segment) { id ⇒
      logger.info(s"Looking Up ID $id")
      onSuccess(CIM.lookupUserIdMappingById(id)) {
        case Some(u) ⇒
          logger.info(s"Found $u"); complete(StatusCodes.OK → u)
        case None ⇒ logger.info("Not Found"); complete(StatusCodes.NotFound → ("result" → s"User $id not found"))
      }
    } ~
      path("users") {
        parameters("connectid") { id ⇒
          onSuccess(CIM.lookupUserIdMappingByConnect(id)) {
            case Some(um) ⇒ complete(StatusCodes.OK → um)
            case None ⇒ complete(StatusCodes.NotFound → ("result" → s"No UserMapping for Connect $id"))
          }
        } ~
          parameters("facebookid") { id ⇒
            onSuccess(CIM.lookupUserIdMappingByFacebook(id)) {
              case Some(um) ⇒ complete(StatusCodes.OK → um)
              case None ⇒ complete(StatusCodes.NotFound → ("result" → s"No Entity for Facebook $id"))
            }
          }
      }
  }
  val update = put {
    path("users" / Segment) { id ⇒
      entity(as[UserIdMapping]) { newUser ⇒
        // Want to assert id = newUser.localid
        onComplete(CIM.updateUserIdMapping(newUser)) {
          case Success(1) ⇒ complete(StatusCodes.OK → newUser)
          case Success(0) ⇒ complete(StatusCodes.NotFound → newUser)
          case Failure(e: PSQLException) ⇒ // 23505 really
            val err = DBError.from(e)
            if (err.sqlState == "23505") {
              complete(StatusCodes.BadRequest → err.withMessage("Someone else already bound to External ID"))
            } else {
              complete(500 → err)
            }
          case default ⇒ complete(500 → ("error" → "Internal Server Error"))
        }
      }
    }
  }

  val deletion = delete {
    pathPrefix("users" / Segment) { localid ⇒
      onComplete(CIM.removeUserIdMapping(localid)) {
        case Success(1) ⇒ complete(StatusCodes.OK → ("result" → s"Object Deleted id $localid"))
        case Success(0) ⇒ complete(StatusCodes.NotFound → ("result" → s"Object Not Deleted id $localid"))
        case Success(n) => throw new Exception(s"What the Hell - Deleted $n records for $localid")
        case Failure(ex: PSQLException) ⇒ complete(500 → DBError.from(ex).withMessage("Could not delete user"))
        case default ⇒ complete(500 → ("error" → "Internal Server Error"))
      }
    }
  }

  val all = create ~ read ~ update ~ deletion
}
