package com.odenzo.utils.javawebtoken

import com.odenzo.storefront.modules.connect.JWToken
import com.typesafe.scalalogging.LazyLogging
import org.jose4j.jwx.JsonWebStructure
import org.scalatest.FunSuite

class JwtUtils$Test extends FunSuite with LazyLogging {

  // Java Interop. This is just a programming scrachpad really

  val testToken = "eyJhbGciOiJSUzI1NiIsImtpZCI6IjEifQ.eyJhdXRoX3RpbWUiOjE0NDA3Mzk5MjIsImV4cCI6MTQ0MDc0MzgyNiwic3ViIjoiNjAzMzk2Njg2MzIwODE2MTI4MCIsImF1ZCI6InRlbGVub3JmaW5hbmNpYWxzZXJ2aWNlcy13YXZlLXdlYiIsImlzcyI6Imh0dHBzOlwvXC9jb25uZWN0LnN0YWdpbmcudGVsZW5vcmRpZ2l0YWwuY29tXC9vYXV0aCIsInRkX3NscyI6dHJ1ZSwiaWF0IjoxNDQwNzM5OTI2LCJhY3IiOiIyIn0.mL9BL5H8wLKpMedaYmSG9h69Cwh8FZdsSjewazez1XaiNV2CyEJFJrSlAqIYw-LF8OyYr6DG8XBgdYzqNhQZCX-7kaRfvmQIx-NgK4ouCoMRNdyvKTK0xsjdgPkyYWPEDTfddqFLIt8lti1Mq-R6e3vEikfezIGBe6PpDfC98AE"

  import scala.collection.JavaConversions._
  test("Unpacking Connect ID Token") {

    val token = new JWToken(testToken)
  }

  def dumpJose(o: JsonWebStructure): Unit = {
    logger.debug(s"Algorithm:  ${o.getAlgorithm} ")
    logger.debug(s"Algorithm Header Val:  ${o.getAlgorithmHeaderValue} ")

    logger.debug(s"Headers:  ${o.getHeaders.getFullHeaderAsJsonString} ")

  }
}
