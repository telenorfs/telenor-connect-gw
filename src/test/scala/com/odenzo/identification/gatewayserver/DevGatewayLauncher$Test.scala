package com.odenzo.identification.gatewayserver

import akka.http.scaladsl.testkit.ScalatestRouteTest
import com.odenzo.storefront.server.OAuthGatewayServer
import com.typesafe.scalalogging.LazyLogging
import org.scalatest.{ FunSuite, Matchers }

class DevGatewayLauncher$Test extends FunSuite with LazyLogging with Matchers with ScalatestRouteTest {

  test("OauthServer Test Run") {
    // Just runs main app with test config and shuts down after a few minutes
    // activator ui probably better way
    // This is my hacking method of old fashioned programming/testing
    logger.warn("Starting OauthServer as daemon")
    OAuthGatewayServer.startServer()
    logger.warn("Sleep time... go ahead and play manually in browser / REST Client UI")
    Thread.sleep(10000000)

    logger.warn("Closing Down")
  }

}
