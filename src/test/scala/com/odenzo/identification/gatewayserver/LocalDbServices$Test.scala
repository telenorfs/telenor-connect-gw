package com.odenzo.identification.gatewayserver

import java.util.UUID

import akka.http.scaladsl.model.StatusCodes
import com.odenzo.storefront.api.ConnectIdServices
import com.odenzo.storefront.api.LocalDbServices
import com.odenzo.storefront.modules.cim.db.UserIdMapping
import org.json4s._
import org.json4s.native.JsonMethods._
import org.scalatest.BeforeAndAfterAll

class LocalDbServices$Test extends GatewayServerTestSpec with BeforeAndAfterAll {

  // Okay -- my understanding is that this doesn't actually spin up an HTTP Server

  val testroute = ConnectIdServices.all

  // A Before All to Create a Unique User for Query Tests

  val userROall = genSafeUser

  override def beforeAll() = {
    Post("/users", userROall) ~> LocalDbServices.all ~> check {
      val body = responseAs[JObject]
      logger.debug(s"\n Created Before RO => ${dump(body)}\n")
      response.status shouldBe StatusCodes.Created
    }
  }

  test("ping") {
    Get("/ping") ~> LocalDbServices.all ~> check {
      response.status shouldBe StatusCodes.OK
      val x = responseAs[JObject]
      logger.debug(s"Got Ping: [$x]")

    }
  }

  test("Create Unique User") {
    val user = genSafeUser
    Post("/users", user) ~> LocalDbServices.all ~> check {
      val body = responseAs[JObject]
      logger.info(s"\n\n PRETTY: ${pretty(render(body))}\n\n")
    }
  }

  test("Create Duplicate User") {

    Post("/users", userROall) ~> LocalDbServices.all ~> check {
      val body = responseAs[JObject]
      logger.info(s"\nPRETTY: ${dump(body)}\n")
      response.status shouldBe StatusCodes.BadRequest // Not sure this is best code
    }
  }

  test("Lookup Local Id") {
    Get(s"/users/${userROall.local}") ~> LocalDbServices.all ~> check {

      val x = responseAs[UserIdMapping]
      logger.debug(s"R = $x")
      response.status shouldBe StatusCodes.OK
    }
  }

  test("Lookup Missing") {
    Get("/users/111") ~> LocalDbServices.all ~> check {
      val body = responseAs[JObject]
      logger.info(s"\n Missing: ${dump(body)}\n")
      response.status shouldBe StatusCodes.NotFound
    }
  }

  test("Lookup By Connect Id") {
    val id = userROall.connect.get
    Get(s"/users?connectid=$id") ~> LocalDbServices.all ~> check {
      val body = responseAs[JObject]
      logger.info(s"\n Lookup => ${dump(body)}\n")
      response.status shouldBe StatusCodes.OK
      val x = responseAs[UserIdMapping]
      x.connect shouldBe Some(id)
      logger.debug(s"R = $x")
    }
  }

  test("Lookup By Facebook Id") {
    val id = userROall.facebook.get
    Get(s"/users?facebookid=$id") ~> LocalDbServices.all ~> check {
      val body = responseAs[JObject]
      logger.info(s"\n Lookup => ${dump(body)}\n")
      response.status shouldBe StatusCodes.OK
      val x = responseAs[UserIdMapping]
      x.facebook shouldBe Some(id)
      logger.debug(s"R = $x")
    }
  }

  test("Update User") {
    val user = genSafeUser
    val changed = user.copy(connect = None, facebook = Some(UUID.randomUUID().toString))

    Post("/users", user) ~> LocalDbServices.all ~> check {
      val body = responseAs[JObject]
      logger.info(s"Created => ${dump(body)}")
    }

    Put(s"/users/${user.local}", changed) ~> LocalDbServices.all ~> check {
      logger.info(s"Status Code: " + response.status)
      logger.info("Updated => " + dump(responseAs[JObject]))
    }
    // Because of my sloppy coding of not returning actual updated object check by a query
    Get(s"/users/${changed.local}") ~> LocalDbServices.all ~> check {
      response.status shouldBe StatusCodes.OK
      val res = responseAs[UserIdMapping]
      res.local shouldBe user.local
      res.local shouldBe changed.local
      res.connect shouldBe changed.connect
      res.facebook shouldBe changed.facebook
    }
  }

  test("Update Non-Existing User") {
    val user = genSafeUser
    Put(s"/users/${user.local}", user) ~> LocalDbServices.all ~> check {
      logger.info(s"Status Code: " + response.status)
      logger.info("Updated => " + dump(responseAs[JObject]))
      response.status shouldBe StatusCodes.NotFound
    }
  }

  test("Update with Constraint Violation") {
    val user = genSafeUser
    Post("/users", user) ~> LocalDbServices.all ~> check {
      val body = responseAs[JObject]
      logger.info(s"Created => ${dump(body)}")
    }

    val changed = user.copy(connect = userROall.connect)
    Put(s"/users/${user.local}", changed) ~> LocalDbServices.all ~> check {
      logger.info(s"Status Code: " + response.status)
      logger.info("Updated => " + dump(responseAs[JObject]))
    }
  }

  test("Delete  Missing") {
    Delete("/users/111") ~> LocalDbServices.all ~> check {
      val body = responseAs[JValue]
      logger.debug("Result: " + dump(body))
      response.status shouldBe StatusCodes.NotFound

    }
  }

  test("Create and Delete") {
    val user = genSafeUser
    Post("/users", user) ~> LocalDbServices.all ~> check {
      response.status shouldBe StatusCodes.Created
    }
    Delete(s"/users/${user.local}") ~> LocalDbServices.all ~> check {
      val body = responseAs[JObject]
      logger.debug("Rsult: " + dump(body))
      response.status shouldBe StatusCodes.OK
    }
  }

}
