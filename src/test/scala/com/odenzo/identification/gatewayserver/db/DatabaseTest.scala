package com.odenzo.identification.gatewayserver.db

import com.odenzo.identification.gatewayserver.GatewayServerTestSpec
import com.odenzo.storefront.modules.cim.db.CIM
import org.postgresql.util.PSQLException
import org.scalatest.time.{ Seconds, Span }

class DatabaseTest extends GatewayServerTestSpec {

  override implicit val timeouts = PatienceConfig(timeout = Span(20, Seconds), interval = Span(.1, Seconds))
  val db = CIM
  logger.info("PrototypeDB: " + db.db)

  test("Database Connectivity Test") {
    val answer = db.lookupUserIdMappingById("NotThere").futureValue
    logger.info("Not Found: " + answer)
    answer shouldBe empty
  }

  test("Get Existing User") {
    val nUser = genSafeUser
    db.createUserIdMapping(nUser).futureValue shouldBe 1
    val answer = db.lookupUserIdMappingById(nUser.local).futureValue
    answer shouldBe defined
    answer.get.local shouldBe nUser.local
    answer.get.facebook shouldBe nUser.facebook
    answer.get.connect shouldBe nUser.connect
    db.removeUserIdMapping(nUser.local).futureValue shouldBe 1
  }

  test("Insert A User and update it") {
    val nUser = genSafeUser
    val upUser = genSafeUser.copy(local = nUser.local)
    db.createUserIdMapping(nUser).futureValue shouldBe 1
    val user = db.lookupUserIdMappingById(nUser.local).futureValue
    user shouldBe defined
    user.get shouldEqual nUser

    db.updateUserIdMapping(upUser).futureValue shouldBe 1
    val updatedUser = db.lookupUserIdMappingById(nUser.local).futureValue
    updatedUser shouldBe defined
    updatedUser.get shouldEqual upUser

  }

  test("Update Changing a Value to NULL") {
    val nUser = genSafeUser
    val upUser = genSafeUser.copy(local = nUser.local, connect = None)

    db.createUserIdMapping(nUser).futureValue shouldBe 1
    db.updateUserIdMapping(upUser).futureValue shouldBe 1
    val updated = db.lookupUserIdMappingById(nUser.local).futureValue
    updated shouldBe defined
    updated.get.connect shouldBe None
  }

  test("Delete An Existing User") {
    val nUser = genSafeUser
    db.createUserIdMapping(nUser).futureValue shouldBe 1
    db.removeUserIdMapping(nUser.local).futureValue shouldBe 1
  }

  test("Delete A Non Existing User") {
    val nUser = genSafeUser
    db.removeUserIdMapping(nUser.local).futureValue shouldBe 0

  }

  test("Primary Key (local) Should Fail with SQLState 2305") {
    val nUser = genSafeUser
    db.createUserIdMapping(nUser).futureValue shouldBe 1
    db.createUserIdMapping(nUser).failed.futureValue match {
      case dbErr: PSQLException =>
        logger.info(s"State ${dbErr.getSQLState}")
        dbErr.getSQLState shouldBe "23505"
      case other => other shouldBe a[PSQLException]
    }
  }

  test("Connect ID Constraint Violation") {
    val nUser = genSafeUser
    val cbu = genSafeUser.copy(connect = nUser.connect)
    db.createUserIdMapping(nUser).futureValue shouldBe 1
    db.createUserIdMapping(cbu).failed.futureValue match {
      case dbErr: PSQLException =>
        logger.info(s"Expected Err: " + dbErr + " Column = " + dbErr.getServerErrorMessage.getConstraint)
        logger.info(s"Connect ID Constraint Violation State ${dbErr.getSQLState}")
        dbErr.getSQLState shouldBe "23505"
      case other => other shouldBe a[PSQLException]
    }
  }
}
