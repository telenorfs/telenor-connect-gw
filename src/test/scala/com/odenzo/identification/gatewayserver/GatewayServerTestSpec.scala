package com.odenzo.identification.gatewayserver

import java.util.UUID

import akka.http.scaladsl.testkit.ScalatestRouteTest
import com.odenzo.storefront.modules.cim.db.UserIdMapping
import com.typesafe.scalalogging.LazyLogging
import de.heikoseeberger.akkahttpjson4s.Json4sSupport
import org.json4s.native.JsonMethods
import org.json4s.DefaultFormats
import org.json4s.JValue
import org.json4s.native
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.time.Seconds
import org.scalatest.time.Span
import org.scalatest.FunSuite
import org.scalatest.Matchers

trait GatewayServerTestSpec extends FunSuite
    with Matchers with ScalaFutures with ScalatestRouteTest with Json4sSupport with LazyLogging {

  implicit val serialization = native.Serialization
  implicit val formats = DefaultFormats

  import org.json4s.JsonDSL._

  implicit val timeouts = PatienceConfig(timeout = Span(20, Seconds), interval = Span(.1, Seconds))

  def genSafeUser: UserIdMapping = {
    val id = UUID.randomUUID().toString
    val connect = UUID.randomUUID().toString
    val facebook = UUID.randomUUID().toString
    UserIdMapping(id, Some(connect), Some(facebook))
  }

  def dump(d: JValue) = JsonMethods.pretty(JsonMethods.render(d))

  def toJson(u: UserIdMapping) = ("local" -> u.local) ~ ("connect" -> u.connect) ~ ("facebook" -> u.facebook)

}
