package com.odenzo.storefront.modules.connect.gbe.testsupport

import com.odenzo.storefront.modules.connect.gbe.id.AuthenticationServerTestSpec
import com.typesafe.scalalogging.LazyLogging

/**
 * It is a pain to get auth token, access token, refresh token and not have them change.
 * So, this keeps some MUTABLE state that a series of tests can keep up to date.
 * Note that using this implies you cannot run tests in parallel.
 */
object TokenStateForTests extends LazyLogging {

  import net.ceedubs.ficus.Ficus._

  /** Original Static Tokens **/
  val tokens = ConnectIdTestConfig.tokens

  /**
   * Latest Access Token (dynamic / mutable)
   */
  var accessTokenOpt: Option[String] = tokens.getAs[String]("access_token")
  var refreshTokenOpt: Option[String] = tokens.getAs[String]("refresh_token")

  logger.warn(s"Initial Test Token State $accessTokenOpt and $refreshTokenOpt")

  def refreshToken = refreshTokenOpt.get

  def accessToken: String = accessTokenOpt.get

  val authToken = ConnectIdTestConfig.configByMode.getString("telenor.connect.auth.code")
  logger.info(s"Auth Token $authToken")
  //  private def GetOrDie[T](errMsg:String)( _ = {
  // catch no such element error and re-throw with message
  //         .fold(throw new IllegalStateException("INVALID_REFRESH_TOKEN"))(_)
  //  }

}

class TestPrimerData extends AuthenticationServerTestSpec {

  import akka.http.scaladsl.model.Uri

  test("Getting valid auth token easily") {
    logger.info(s"Env Mode: ${ConnectIdTestConfig.envMode}")
    val host = "staging.telenordigital.com"

    val scopes = Seq("profile", "openid", "telenorfinancialservices.wave")

    val params = Uri.Query(
      "response_type" → "code", // No Need to change this
      "client_id" → clientId, // Issues by Telenor Digital
      "redirect_uri" → oauthCallback, // Where redirect redirects back too. Based on env and SSO check or real login
      "scope" → scopes.mkString(" "), // These are the OAuth grant scope we ask for
      "state" -> "bliss"
    )

    // Because we will need to store cookies I am not sure how to do this with curl or wget
    // Guess I could try Applescripting it for the ultimate of stupid hacks
    logger.debug("Query Params: " + params)
    val uri = Uri(oauthServer + "/oauth/authorize").withQuery(params).toString()
    logger.debug(s"URL -- Slow way is to just click the URL, you may then get error because localhost:9443 is not running (unless it is!):\ncurl -c $uri\n")
    logger.debug("Copy the code=[CODE]  CODE part and that is your auth token for testing. May as well just paste it in ")
  }

}
