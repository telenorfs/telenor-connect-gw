package com.odenzo.storefront.modules.connect.gbe.id.authentication

import com.odenzo.storefront.modules.connect.gbe.id.AuthenticationServerTestSpec
import com.odenzo.storefront.modules.connect.gbe.testsupport.TokenStateForTests
import org.json4s.JsonAST.JString
import org.scalatest.CancelAfterFailure

// TODO: Make this take a test parameter
class RefreshAccessToken$Test extends AuthenticationServerTestSpec with CancelAfterFailure {

  val refreshToken = TokenStateForTests.refreshTokenOpt.getOrElse("INVALID_REFRESH_TOKEN")
  test("Refresh Token Action", RefreshTokenTestTag) {
    logger.debug(s"Trying to Refresh Token $refreshToken")
    val json = RefreshAccessToken.refreshAccessToken(refreshToken).futureValue

    logger.info(s"Answer ${dump(json)}")

    val JString(nAccessToken) = json \ "access_token"
    val JString(nRefreshToken) = json \ "refresh_token"
    logger.debug(s"Updates Access $nAccessToken and Reresh $nRefreshToken in test state")
    TokenStateForTests.accessTokenOpt = Some(nAccessToken)
    TokenStateForTests.refreshTokenOpt = Some(nRefreshToken)

  }

}
