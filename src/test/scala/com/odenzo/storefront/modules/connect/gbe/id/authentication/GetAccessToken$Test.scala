package com.odenzo.storefront.modules.connect.gbe.id.authentication

import com.odenzo.storefront.modules.connect.gbe.id.AuthenticationServerTestSpec
import com.odenzo.storefront.modules.connect.gbe.testsupport.TokenStateForTests

/**
 * Note that this requires an Auth Token - so not easy to test automatically
 * Calls resolve_auth_token
 */
class GetAccessToken$Test extends AuthenticationServerTestSpec {

   val staging =  "https://connect.ripplewaves.com/connect/oauth2callback"
  val local = "https://localhost:9443/connect/oauth2callback"
  val testCallbackUrl = local
  // RefreshTokenTest and AccessTokenTest are tags :-)
  // Not sure what I was thinking making Suites this way
  // This requires an auth token
  test("GetAccessToken", RefreshTokenTestTag, AccessTokenTestTag) {
    val authToken = "DontPasteHereAndItWillFailOnPurpose"
    val err = GetAccessToken(authToken, testCallbackUrl).failed.futureValue

    logger.info(s"Err Answer:", err)
  }

  // Forgot what this ScalaTest multiple test things does. Ordered Suite?
  test("GetAccessToken from Auth") {
    val authToken = TokenStateForTests.authToken
    val json = GetAccessToken(authToken, testCallbackUrl).futureValue

    logger.info(s"JSON Answer: ${dump(json)}")
  }

  // Forgot what this ScalaTest multiple test things does. Ordered Suite?
  test("GetAccessToken from Auth Pasted Into Test") {
    val authToken = "ufRNp4qqkx5cz9nFUW4zA3hucWU"
    val json = GetAccessToken(authToken, testCallbackUrl).futureValue

    logger.info(s"JSON Answer: ${dump(json)}")
    (json \ "refresh_token").toOption shouldBe defined
    (json \ "access_token").toOption shouldBe defined
    (json \ "id_token").toOption shouldBe defined // Assuming openid scope always used by convention

  }

}
