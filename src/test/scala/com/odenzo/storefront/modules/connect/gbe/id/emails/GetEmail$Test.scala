package com.odenzo.storefront.modules.connect.gbe.id.emails

import com.odenzo.storefront.modules.connect.gbe.id.AuthenticationServerTestSpec
import com.odenzo.storefront.modules.connect.gbe.testsupport.TokenStateForTests

class GetEmail$Test extends AuthenticationServerTestSpec {

  val accessToken = TokenStateForTests.accessToken

  val emailObjectId = "6033968052029100032"

  test("Good email") {
    val json = GetEmail.get(sub, emailObjectId).futureValue
    logger.debug("List of Emails: " + dump(json))
  }
}
