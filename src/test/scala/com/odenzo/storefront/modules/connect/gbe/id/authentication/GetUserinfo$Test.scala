package com.odenzo.storefront.modules.connect.gbe.id.authentication

import com.odenzo.storefront.modules.connect.gbe.id.AuthenticationServerTestSpec
import com.odenzo.storefront.modules.connect.gbe.testsupport.TokenStateForTests

class GetUserinfo$Test extends AuthenticationServerTestSpec {

  val accessToken = TokenStateForTests.accessTokenOpt.getOrElse("INVALID_ACCESS_TOKEN")

  test("getUserinfo", AccessTokenTestTag) {

    logger.debug(s"get_user_info with  Access Token $accessToken")
    val reply = GetUserinfo.getUserinfo(accessToken = accessToken)
    val answer = reply.futureValue
    logger.info(s"JSON Answer: $answer")
  }

}
