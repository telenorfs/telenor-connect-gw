package com.odenzo.storefront.modules.connect.gbe.id.authentication

import com.typesafe.scalalogging.LazyLogging
import org.scalatest.CancelAfterFailure
import org.scalatest.Suite
import org.scalatest.Suites

/**
 * The intent of this Suite is to call all authentication tests possible that are accessible with a refresh token.
 * The refresh token has to be able to get an access token or some tests are blocked.
 * None of these tests should invalidate the refresh token!
 */
class AuthenticationSuite extends Suites with LazyLogging with CancelAfterFailure {

  // Can realistically only have one auth token
  val authToken = "RDtADz6iOe7tkoUGC2GDCpQEcxW"

  val noPreReqs = Seq(
    new GetJwkKeyset$Test("Hello"),
    new GetOpenidConnectConfiguration$Test
  )



  /**
   * These require an access token -- need to define where they get it from
   */
  val accessTokenReq = Seq(

    new VerifyToken$Test("idToken", "accessTokenToFillIn"),
    new GetUserinfo$Test
  )

  /**
   * Require a refresh token -- but still cannot destroy it
   */
  val refreshTokenReq = Seq()

  /**
   * Tests that require an Auth Token.
   */
  val authTokenRes = Seq(
    new GetAccessToken$Test
  )

  /**
   * Set this to list of things to run.
   */
  val ordered = noPreReqs

  override val nestedSuites: collection.immutable.IndexedSeq[Suite] = Vector.empty ++ ordered

}
