package com.odenzo.storefront.modules.connect.gbe.id.authentication

import com.odenzo.storefront.modules.connect.gbe.id.AuthenticationServerTestSpec

class GetOpenidConnectConfiguration$Test extends AuthenticationServerTestSpec {

  test("Config") {
    val future = GetOpenidConnectConfiguration()
    val answer = future.futureValue // Block ....
    val fields = answer.values
    fields should contain key "token_endpoint"
  }
}
