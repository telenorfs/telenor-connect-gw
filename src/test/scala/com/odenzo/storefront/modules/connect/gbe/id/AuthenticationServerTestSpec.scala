package com.odenzo.storefront.modules.connect.gbe.id

import akka.actor.ActorSystem
import com.odenzo.storefront.modules.connect.gbe.testsupport.ConnectIdTestConfig
import com.typesafe.scalalogging.LazyLogging
import net.ceedubs.ficus.Ficus._
import org.json4s._
import org.json4s.native.JsonMethods
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.time.Seconds
import org.scalatest.time.Span
import org.scalatest.FunSuite
import org.scalatest.Matchers

trait AuthenticationServerTestSpec extends FunSuite with LazyLogging with Matchers with ScalaFutures with ConnectIdTestConfig {
  // Test for Connect ID stuff that don't need to fire up actor system

  import org.scalatest.Tag

  implicit val serialization = native.Serialization
  implicit val formats = DefaultFormats

  object AuthTokenTest extends Tag("com.odenzo.tags.AuthToken")

  // Need a Valid AuthToken to Run
  object AccessTokenTestTag extends Tag("com.odenzo.tags.AccessToken")

  // Needs Valid Access Token
  object RefreshTokenTestTag extends Tag("com.odenzo.tags.RefreshToken")

  // Needs Valid Access Token
  object SPPasswordTest extends Tag("com.odenzo.tags.SPPassword")

  // Needs Valid Access Token

  implicit val system = ActorSystem()
  implicit val defaultPatience = PatienceConfig(timeout = Span(20, Seconds), interval = Span(5, Seconds))

  // Never do this: import system.dispatcher
  implicit val ec = scala.concurrent.ExecutionContext.Implicits.global

  val account = connectConfig.getAs[String]("steve.account").getOrElse("NO_TESTING_ACCOUNT_CONFIGED")
  val sub = connectConfig.getAs[String]("steve.sub").getOrElse("NO_TESTING_SUB_CONFIGED")
  val idtoken = connectConfig.getAs[String]("steve.id").getOrElse("NO_TESTING_ID_CONFIGED")


  def dump(d: JValue) = JsonMethods.pretty(JsonMethods.render(d))

  def defaultFailurePF[T]: PartialFunction[T, Unit] = {
    case err: Throwable ⇒ logger.error("Failed with Exception: ", err)
  }


}
