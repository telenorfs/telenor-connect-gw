package com.odenzo.storefront.modules.connect.gbe.id.users

import com.odenzo.storefront.modules.connect.gbe.id.AuthenticationServerTestSpec
import net.ceedubs.ficus.Ficus

class GetUserById$Test extends AuthenticationServerTestSpec {

  import Ficus._

  val phone = authReply.as[String]("userinfo.phone_number")

  test("positive") {
    logger.info(s"Getting User by Phone $phone")
    val reply = GetUserByUserId.get(sub)
    // TODO: Complete this test
    reply.onFailure {
      case e ⇒ logger.error("UnHandled Exception In Call:", e)
    }

    val answer = reply.futureValue
    logger.info(s"JSON Answer: $answer")
  }
}
