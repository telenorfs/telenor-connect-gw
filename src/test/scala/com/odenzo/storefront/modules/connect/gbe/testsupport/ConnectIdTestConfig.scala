package com.odenzo.storefront.modules.connect.gbe.testsupport

import com.odenzo.storefront.modules.connect.ConnectIdConfig
import com.typesafe.config.Config
import com.typesafe.scalalogging.LazyLogging

/**
 * Things from application config that will only be found in test code.
 *
 */
trait ConnectIdTestConfig extends ConnectIdConfig with LazyLogging {

  val authReply = configByMode.getConfig("telenor.connect.auth.sample.login")

  val tokens: Config = authReply.getConfig("base")

  val userinfo: Config = authReply.getConfig("userinfo") //.as[UserInfo]

}

object ConnectIdTestConfig extends ConnectIdTestConfig
