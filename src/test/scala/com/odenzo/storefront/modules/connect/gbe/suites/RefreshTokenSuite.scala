package com.odenzo.storefront.modules.connect.gbe.suites

import com.odenzo.storefront.modules.connect.gbe.id.accounts.GetAllAccounts$Test
import com.odenzo.storefront.modules.connect.gbe.id.authentication.GetAccessToken$Test
import com.odenzo.storefront.modules.connect.gbe.id.authentication.GetUserinfo$Test
import com.odenzo.storefront.modules.connect.gbe.id.authentication.VerifyToken$Test
import com.odenzo.storefront.modules.connect.gbe.id.emails.GetListOfEmails$Test
import com.odenzo.storefront.modules.connect.gbe.id.phones.GetListOfPhones$Test
import com.typesafe.scalalogging.LazyLogging
import org.scalatest.CancelAfterFailure
import org.scalatest.Suite
import org.scalatest.Suites

/**
 * The intent of this Suite is to call all authentication tests possible that are accessible with a refresh token.
 * The refresh token has to be able to get an access token or some tests are blocked.
 * None of these tests should invalidate the refresh token!
 */
class RefreshTokenSuite extends Suites with LazyLogging with CancelAfterFailure {

  /**
   * These all require a valid connect id
   */
  val connectIdReq = Seq(
    new GetAllAccounts$Test,
    new GetListOfEmails$Test,
    new GetListOfPhones$Test
  )
  /**
   * These require an access token -- need to define where they get it from
   */
  val accessTokenReq = Seq(
    new VerifyToken$Test("FakeToken", "FakeToken"),
    new GetUserinfo$Test
  )
  /**
   * Require a refresh token -- but still cannot destroy it
   */
  val refreshTokenReq = Seq()

  /**
   * Tests that require an Auth Token.
   */
  val authTokenRes = Seq(
    new GetAccessToken$Test
  )
  /**
   * Set this to list of things to run.
   */
  val ordered = connectIdReq ++ accessTokenReq

  override val nestedSuites: collection.immutable.IndexedSeq[ Suite ] = Vector.empty ++ ordered
}
