package com.odenzo.storefront.modules.connect.gbe.id.users

import akka.http.scaladsl.model.Uri.{ ParsingMode, Query }
import akka.http.scaladsl.model.{ FormData, HttpRequest, Uri }
import com.odenzo.storefront.modules.connect.gbe.id.AuthenticationServerTestSpec
import com.odenzo.storefront.modules.connect.gbe.testsupport.TokenStateForTests
import net.ceedubs.ficus.Ficus

class GetUserByUserName$Test extends AuthenticationServerTestSpec {

  import Ficus._

  val phone = authReply.as[String]("userinfo.phone_number")

  val accessToken = TokenStateForTests.accessToken

  test("getUserByUserName Telephone", SPPasswordTest) {
    logger.info(s"Getting User by Phone $phone")
    val json = GetUserByUserName.exec(phone, accessToken).futureValue
    logger.info(s"JSON Answer: ${dump(json)}")
  }

  test("Bad Phone Telephone", SPPasswordTest) {
    logger.info(s"Getting User by Phone $phone")
    val reply = GetUserByUserName.exec("66896666666", accessToken).futureValue
    logger.info(s"JSON Answer: ${dump(reply)}")
  }

  test("getUserByUserName email", SPPasswordTest) {
    val reply = GetUserByUserName.exec("test@odenzo.com", accessToken).futureValue
    logger.info(s"JSON Answer: ${dump(reply)}")
  }

  test("URI Encoding") {
    // These things store not url-encoded so logging not url-encoded
    // Unsure how to check the final rendered on the wire

    val param = "test@foo.com"
    val encoded = "test%40foo.com"

    val form = FormData(Query(param))
    logger.info(s"Query via form ${form.toString}")
    val uri = Uri("http://nowhere.com").withQuery(Uri.Query(param, mode = ParsingMode.Strict))
    HttpRequest().withUri(uri)
    logger.debug("URI " + uri.toString())
    val q = Query(param)
    logger.debug(s"Query Value ${q.value} and toString ${q.toString()}")

    q.value should equal(encoded)
  }
}
