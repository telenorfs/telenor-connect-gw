package com.odenzo.storefront.modules.connect.gbe.id.authentication

import com.odenzo.storefront.modules.connect.gbe.id.AuthenticationServerTestSpec

class GetJwkKeyset$Test(val msg: String) extends AuthenticationServerTestSpec {

  logger.info("JWK Message: " + msg)
  test("Java Web Token") {
    val json = GetJwkKeyset.fetch().futureValue
    logger.info("JSON: " + dump(json))
    (json \ "keys").children.length should be >= 1

  }
}
