package com.odenzo.storefront.modules.connect.gbe.id.phones

import com.odenzo.storefront.modules.connect.gbe.id.AuthenticationServerTestSpec

class GetListOfPhones$Test extends AuthenticationServerTestSpec {

  test("Good ConnectID") {
    val json = GetListOfPhones.get(sub).futureValue
    logger.debug("List of Phones: " + dump(json))
  }
}
