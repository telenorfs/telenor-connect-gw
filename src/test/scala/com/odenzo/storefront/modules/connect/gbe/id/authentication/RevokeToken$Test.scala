package com.odenzo.storefront.modules.connect.gbe.id.authentication

import com.odenzo.storefront.modules.connect.gbe.id.AuthenticationServerTestSpec
import com.odenzo.storefront.modules.connect.gbe.testsupport.TokenStateForTests

class RevokeToken$Test extends AuthenticationServerTestSpec {

  val refreshToken = TokenStateForTests.refreshTokenOpt.getOrElse("INVALID_REFRESH_TOKEN")
  val accessToken = TokenStateForTests.accessTokenOpt.getOrElse("INVALID_ACCESS_TOKEN")
  test("RevokeToken - AccessToken", RefreshTokenTestTag, AccessTokenTestTag) {
    logger.debug(s"get_user_info with Refresh $refreshToken and Access Token $accessToken")
    val json = RevokeToken.revoke(accessToken).futureValue
    logger.info(s"JSON Answer: $json")
  }

}
