package com.odenzo.storefront.modules.connect.gbe.id.accounts

import com.odenzo.storefront.modules.connect.gbe.id.AuthenticationServerTestSpec

class CreateNewAccount$Test extends AuthenticationServerTestSpec {

  test("Creating a New Account") {
    val answer = CreateNewAccount.create(sub, "FSU", "fakeaccount").futureValue
    logger.debug(s"Answer = ${dump(answer)}")
  }

}
