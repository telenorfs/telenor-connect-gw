package com.odenzo.storefront.modules.connect.gbe.id.authentication

import com.odenzo.storefront.modules.connect.gbe.id.AuthenticationServerTestSpec

/**
 * The "id token" is JWT, Java Web Token.
 * This is kind of stand-alone, although to check signature we need to access ConnectID server
 */
class VerifyToken$Test(val idToken: String, val accessToken: String) extends AuthenticationServerTestSpec {

  import net.ceedubs.ficus.Ficus._

  val goodIdToken = authReply.as[String]("tokens.id_token")
  val goodAccessToken = authReply.as[String]("tokens.access_token")

  val badAccessToken = "badaccesstoken"

  val badIdToken = """eyJhbGciOiJSUzI1NiIsImtpZCI6IjEifQ.eyJhdXRoX3RpbWUiOjE0NDAxNjQ5MjcsImV4cCI6MTQ0MDIzNzYwMiwic3ViIjoiNjAzMzk2Njg2MzIwODE2MTI4MCIsImF1ZCI6InRlbGVub3JmaW5hbmNpYWxzZXJ2aWNlcy13YXZlLXdlYiIsImlzcyI6Imh0dHBzOlwvXC9jb25uZWN0LnN0YWdpbmcudGVsZW5vcmRpZ2l0YWwuY29tXC9vYXV0aCIsInRkX3NscyI6dHJ1ZSwiaWF0IjoxNDQwMjMzNzAyLCJhY3IiOiIxIn0.dX55a7ml4CmaZB2fedwvHurHJ4wK5JQm2g3aHMEX24UykG9vKi3ai2YevD68629MmXUZzGlHqmg8UTGLqdqFmUOnSxpegd-t3sWFTVYYvJeZRsazHMhL4l02_IAa8bGLMs-XlSjnYd3RnZriS1O3BT_SWqPI-D3zejW8B7wJKfI"""

  test("Verify Bad ID Token") {

    val reply = VerifyToken.verifyIdToken(badIdToken).futureValue
    reply.fold(b => logger.warn(s"Failed Verification $b "), g => logger.debug(s"Validated Message $g"))
    reply should be('left)

  }

  test("VerifyAccessToken", AccessTokenTestTag) {
    val json = VerifyToken.verifyAccessToken(accessToken).futureValue
    logger.info(s"JSON Answer: $json")
  }

}
