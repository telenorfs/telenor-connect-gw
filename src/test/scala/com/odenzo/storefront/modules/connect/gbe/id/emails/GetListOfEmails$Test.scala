package com.odenzo.storefront.modules.connect.gbe.id.emails

import com.odenzo.storefront.modules.connect.gbe.id.AuthenticationServerTestSpec
import com.odenzo.storefront.modules.connect.gbe.testsupport.TokenStateForTests

class GetListOfEmails$Test extends AuthenticationServerTestSpec {

  val accessToken = TokenStateForTests.accessToken
  test("Bad ConnectID", SPPasswordTest) {
    val json = GetListOfEmails.get("666").futureValue
    logger.debug(s"Email List ${dump(json)}")

  }

  test("Good ConnectID") {
    val json = GetListOfEmails.get(sub).futureValue
    logger.debug("List of Emails: " + dump(json))
  }
}
