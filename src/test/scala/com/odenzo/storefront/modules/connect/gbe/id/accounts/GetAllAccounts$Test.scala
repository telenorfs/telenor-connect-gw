package com.odenzo.storefront.modules.connect.gbe.id.accounts

import com.odenzo.storefront.modules.connect.gbe.id.AuthenticationServerTestSpec

class GetAllAccounts$Test extends AuthenticationServerTestSpec {

  test("Bad ConnectID", SPPasswordTest) {
    val future = GetAllAccounts.get("666")

    // Should get an exception with status 404 in it
    // Still working on Exception structures, lame now
    whenReady(future.failed) { ex ⇒
      logger.debug("Failed ex " + ex.getClass + " -- " + ex)
      ex shouldBe an[Exception]

    }
  }

  test("Good ConnectID") {
    val future = GetAllAccounts.get(sub)
    val answer = future.futureValue
    (answer \ account).extractOpt[String] shouldBe defined
  }
}
