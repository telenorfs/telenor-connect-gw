package com.odenzo.storefront.admin

import java.util.concurrent.TimeUnit

import com.odenzo.storefront.modules.cim.db.CIM
import com.typesafe.scalalogging.LazyLogging
import org.scalatest.FunSuite
import org.scalatest.Matchers
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.time.Span

import scala.concurrent.duration.Duration

class DBAdmin extends FunSuite with LazyLogging with Matchers with ScalaFutures {

  import scala.concurrent.ExecutionContext.Implicits.global

  implicit val config: PatienceConfig = PatienceConfig(Duration(100, TimeUnit.SECONDS))

  test("create_schemas") {
    logger.info(s"Creating CIM Schemas")
    val reply = CIM.createAll
    reply.onFailure {
      case e ⇒ logger.error("UnHandled Exception In Call:", e)
    }

    val answer = reply.futureValue
    logger.info(s"JSON Answer: $answer")
  }

  test("drop_schemas") {
    logger.info(s"Deleting CIM Schemas")
    val reply = CIM.dropAll
    }

  test("dump_schemas") {
    logger.info(s"Creating CIM Schemas")
    val reply = CIM.dumpDDL()

  }
}
