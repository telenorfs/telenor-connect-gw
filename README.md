
# License

This project is Apache 2.0 Open Source licensed.

# Pure Dev Setup

This will run the Postgres Back-End DB in a Docker container and you can run Gateway on host.

      cd src/deploy
      

# Staging Dev Spin-up

This will put the Gateway Server and Postgres running in local docker image.
Most useful for when working on the Play Front-End.

Assumption in my environment:

- Kitematic is running, providing a Docker VM (default)
- You are in a Docker Terminal (DOCKER CLI)  
- docker-compose 1.6.2 latest tested on

    sbt clean  docker:stage
    docker-compose up 




# Telenor Connect ID and GSMA Mobile Connect  Gateway Prototype 

- Instead of documentation and an SDK, I decided to make a "light-weight" or "micro-service" component to help with
Telenor Digital ConnectID integration for FS units.


- It is also an after hours project to see best how to setup devops for Docker/Scala instances on OS X  with SBT
                                                                                                    

# Compilation and Artifact

- To get a  ZIP file or a tgz file (respectively):     

   sbt clean compile universal:packageBin

   sbt clean compile universal:packageZipTarball

- the Zip file will expand into a ./bin and ./lib with bin having a BASH script to launch all the jars in lib
- TGZ is same thing.


# Development Deployment

* Uses SBT, create a project in IntelliJ
* Run the src/deploy/postgres-server Dockerfile, easiest to do setting up IntelliJ run setting

#  Staging Deployment

Is a work in progress, and will actually be moved out to above this project.
***Ignore all the crap below for now****
Currently in src/deploy there is a dockerfile for Postgres Server... I run this and the gateway connects to it.
* All the above assume you are in a Docker Terminal on OS X
* docker-compose.yml is used for most dev type setup.

## Creating Deployment Artifacts

### Approach A: Use docker stuff and build artifacts with SBT
To create a .tgz with all the libraries and executable -- this also creates a run script.

    sbt clean compile universal:package-zip-tarball


### Approach B:
With Docker moving to 
   
    sbt clean compile docker:publishLocal
    
This will through stuff in target/docker/stage/Dockerfile with some things underneath it.
This approach basically generates the Dockerfile from build.sbt stuff.
It fails because the Docker CLI settings don't pass over to SBT.
Thats ok, in fact we want to use  the docker-compose.yml anyway
   
    docker-compose build
 
